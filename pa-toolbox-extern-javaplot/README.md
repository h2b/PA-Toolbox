# PA-Toolbox -- Predictive-Analytics Java Toolbox -- External JavaPlot Library

These source files are copied from an external plotting library named *JavaPlot*. It has not been written by me (the author of the PA-Toolbox project); see [http://javaplot.panayotis.com](http://javaplot.panayotis.com) for the original JavaPlot project. This submodule just serves as backend for the pa-toolbox-plotting implementation and is not meant to compete with the original project.

## License

The original author put his project under the GNU LESSER GENERAL PUBLIC LICENSE.
Although the source file headers mention version 2 of that licence, a copy of 
version 3 is shipped with the original download archive. Also, the [sourceforge project site](https://sourceforge.net/projects/gnujavaplot) explicitly indicates
version 3 (LGPLv3).

It seems to me that eventually the intent of the original author is to use
version 3, so I will treat this code as licenced under the GNU LESSER GENERAL 
PUBLIC LICENSE, version 3.