/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.gson;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author h2b
 *
 */
public class InterfaceAdapterTest {
	
	private static String PROBE = "Hello world!";
	
	private Gson gson;

	private static interface TestInterface {
		
		public String test();

	}

	private static class TestClass implements TestInterface {
		
		private String probe = PROBE;

		@Override
		public String test() {
			return probe;
		}

	}
	
	private static class NestedTestClass {
		
		private TestInterface testField;

		public TestInterface getTestField() {
			return testField;
		}

		public void setTestField(TestInterface testField) {
			this.testField = testField;
		}
		
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		gson = new GsonBuilder().
				registerTypeAdapter(TestInterface.class, new InterfaceAdapter<TestInterface>()).
				create();
	}

	/**
	 * Test method for {@link de.h2b.java.lib.gson.InterfaceAdapter}.
	 */
	@Test
	public void testInterfaceAdapter() {
		TestInterface testObj = new TestClass();
		String json = gson.toJson(testObj);
		TestInterface actual = gson.fromJson(json, TestClass.class);
		assertEquals(PROBE, actual.test());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.gson.InterfaceAdapter}.
	 */
	@Test
	public void testInterfaceAdapterNested() {
		NestedTestClass testObj = new NestedTestClass();
		testObj.setTestField(new TestClass());
		String json = gson.toJson(testObj);
		NestedTestClass actual = gson.fromJson(json, NestedTestClass.class);
		assertEquals(PROBE, actual.getTestField().test());
	}

}
