/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.gson;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author h2b
 *
 */
public class PatternAdapterTest {
	
	private static final String TEST_REGEX = "a*b";
	
	private Pattern pattern;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Gson gson = new GsonBuilder().registerTypeAdapter(Pattern.class, new PatternAdapter()).create();
		String json = gson.toJson(Pattern.compile(TEST_REGEX));
		pattern = gson.fromJson(json, Pattern.class);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.gson.PatternAdapter}.
	 */
	@Test
	public void testPatternAdapter() {
		assertEquals(TEST_REGEX, pattern.pattern());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.gson.PatternAdapter}.
	 */
	@Test
	public void testPatternAdapterMatch() {
		Matcher m1 = pattern.matcher("aaaaab");
		Matcher m2 = pattern.matcher("aaaaxb");
		assertTrue("pattern should match but doesn't", m1.matches());
		assertFalse("pattern shouldn't match but does", m2.matches());
	}

	/**
	 * Note: This appears to be the ultimate test, since it fails if using a
	 * gson without {@code PatternAdapter} while the other tests don't.
	 * <p/>
	 * Test method for {@link de.h2b.java.lib.gson.PatternAdapter}.
	 */
	@Test
	public void testPatternAdapterGroup() {
		Matcher m = pattern.matcher("aaaaab");
		assertEquals(0, m.groupCount());
	}

	/**
	 * Auxiliary test to verify that a gson without {@code PatternAdapter} 
	 * yields a wrong group count.
	 * <p/>
	 * If this test failed, it would not indicate a malfunction of
	 * {@code PatternAdapter} but the potential resolving of a {@code Gson} 
	 * issue. So, remove it in case.
	 * <p/>
	 * Test method for {@link de.h2b.java.lib.gson.PatternAdapter}.
	 */
	@Test
	public void testNoPatternAdapterGroup() {
		Gson plain = new Gson();
		String json = plain.toJson(Pattern.compile(TEST_REGEX));
		Pattern broken = plain.fromJson(json, Pattern.class);
		Matcher m = broken.matcher("aaaaab");
		assertNotEquals(0, m.groupCount());
	}

}
