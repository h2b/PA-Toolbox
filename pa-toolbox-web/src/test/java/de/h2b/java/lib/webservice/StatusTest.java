/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.webservice;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public class StatusTest {

	/**
	 * Verifies that we get correct default messages.
	 */
	@Test
	public void testDefaultMessages() {
		assertEquals("OK", Status.OK.getMessage());
	}

	/**
	 * Verifies different setting of messages.
	 * 
	 * Note that this test may affect {@link #testDefaultMessages} if we are
	 * using the same {@code Status} object.
	 */
	@Test
	public void testSetMessage () {
		String msg = "Unknown error.";
		Status.UNKNOWN_ERROR.setMessage(msg);
		assertEquals(msg, Status.UNKNOWN_ERROR.getMessage());
	}
}
