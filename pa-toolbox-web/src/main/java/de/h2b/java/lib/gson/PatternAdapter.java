/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.gson;

import java.lang.reflect.Type;
import java.util.regex.Pattern;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Provides serialization of {@code Pattern} instances which apparently is 
 * broken in {@code Gson} according to item 380 of the google-gson issues page.
 * 
 * @see <a href="https://code.google.com/p/google-gson/issues/detail?id=380">Issue 380</a>
 * 
 * @author h2b
 *
 */
public class PatternAdapter implements JsonSerializer<Pattern>,
		JsonDeserializer<Pattern> {
	
	private static final String PROPNAME_REGEX = "regex";
	
	private static final String PROPNAME_FLAGS = "flags";

	@Override
	public JsonElement serialize(Pattern src, Type typeOfSrc,
			JsonSerializationContext context) {
		final JsonObject wrapper = new JsonObject();
		wrapper.addProperty(PROPNAME_REGEX, src.pattern());
		wrapper.addProperty(PROPNAME_FLAGS, src.flags());
		return wrapper;
	}

	@Override
	public Pattern deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		final String regex = get(json, PROPNAME_REGEX).getAsString();
		final int flags = get(json, PROPNAME_FLAGS).getAsInt();
		return Pattern.compile(regex, flags);
	}
	
	private JsonElement get(final JsonElement element, String propname) {
		final JsonElement result = element.getAsJsonObject().get(propname);
		if (result==null) {
			throw new JsonParseException(this.getClass() + " missing property: " + propname);
		}
		return result;
	}

}
