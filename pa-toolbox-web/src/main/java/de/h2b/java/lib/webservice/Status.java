/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.webservice;

/**
 * Constants indicating some condition.
 * 
 * @author h2b
 *
 */
public enum Status {
	
	/**
	 * Everything is all right.
	 */
	OK (0, null),
	
	/**
	 * Something went wrong. Reason is unclear.
	 */
	UNKNOWN_ERROR (1, null),
	
	/**
	 * Access could not be authenticated.
	 */
	ACCESS_DENIED (2, null),
	
	/**
	 * Some database operation could not be accomplished.
	 */
	DB_ERROR (3, null),
	
	/**
	 * Information requested could not be found.
	 */
	NOT_FOUND (4, null),
	
	/**
	 * A result that has been expected to be unique is ambiguous.
	 */
	NOT_UNIQUE (5, null),
	
	/**
	 * A value has an invalid format or type.
	 */
	INVALID (6, null),
	
	/**
	 * An input or output operation failed.
	 */
	IO_ERROR (7, null);
	
	/**
	 * A numeric code associated with the state. 
	 */
	private int code;
	
	/**
	 * A message associated with the state. 
	 */
	private String message;

	/**
	 * Constructs a new {@code Status} element.
	 * 
	 * It is guaranteed to have a non-null, non-empty message after creation, 
	 * even if the string specified is {@code null} or empty.
	 * 
	 * @param code
	 * @param message
	 */
	private Status(int code, String message) {
		this.code = code;
		this.message = 
				(message==null || message.length()==0)? this.name(): message;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets a new message for this {@code Status} object. Note that this object
	 * is a singleton, so that setting a new message may have side effects.
	 * 
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
