# PA-Toolbox -- Predictive-Analytics Java Toolbox

This is a Java library of some tools that happened to be useful in a predictive-analytics environment. Though, most of them are not specific to this area and might be of general interest.

For separation of concerns and minimizing dependencies this project has been divided into several sub modules:

* *pa-toolbox-evaluation* offers some statistical methods for vector and matrix data defined by the pa-toolbox-linalg module.

* *pa-toolbox-linalg* provides data types and operations for algebraic vectors and matrices. The interfaces have generic element types, while the implementations are limited to elements of type `Double` for now. You might also consider to use my [LinAlg](https://gitlab.com/h2b/LinAlg) Scala Library instead, since it has much more functionality.

* *pa-toolbox-office* contains some classes to deal with Excel files. They are based on [Apache POI](https://poi.apache.org/). If your application makes heavy use of data input and/or output in Excel format, this module can save you lots of lines of code.

* *pa-toolbox-util* encloses various utilities. Some highlights are a data cleaner (which can eliminate obviously wrong data from or interpolate missing elements of a list of data) and a statistics accumulator (which is very handy when error counters or other statistics have to be transferred between program units).

* *pa-toolbox-web* provides some adapters for JSON processing with [Gson](https://github.com/google/gson) and some simple classes to unify status codes for web services.

* *pa-toolbox-plotting* constitutes a plotting framework for 2D and 3D curves based on vector and matrix data provided by the pa-toolbox-linalg module. To use this submodule, [gnuplot](http://gnuplot.info) must be installed on the system where the final executable runs.

* *pa-toolbox-extern-javaplot* is an external plotting library using [gnuplot](http://gnuplot.info) as generator. It has not been written by me (the author of the PA-Toolbox project); see [http://javaplot.panayotis.com](http://javaplot.panayotis.com) for the original JavaPlot project. This submodule just serves as backend for the pa-toolbox-plotting implementation and is not meant to compete with the original project.

There is also a Scala interface for some sub modules, currently for *pa-toolbox-linalg* and *pa-toolbox-plotting*. It simplifies the use of these modules with Scala in general and my [LinAlg](https://gitlab.com/h2b/LinAlg) Scala Library in particular.

Note that the Scala interface is not included in the Maven build procedure of the main project; it has its own sbt build procedure instead. So, to generate the Scala classes from source use sbt from the `scala-interface` folder.

## API

For the main Java project see [Javadoc](http://hans-hermann-bode.de/sites/default/files/sites/pa-toolbox/apidocs/index.html).

For the Scala interface see:

* *pa-toolbox-linalg* -> [Scaladoc](http://hans-hermann-bode.de/sites/default/files/sites/pa-toolbox/scaladocs/pa-toolbox-linalg/de/h2b/scala/lib/pa_toolbox/math/linalg/index.html)

* *pa-toolbox-plotting* -> [Scaladoc](http://hans-hermann-bode.de/sites/default/files/sites/pa-toolbox/scaladocs/pa-toolbox-plotting/de/h2b/scala/lib/pa_toolbox/data/plotting/index.html)

## Maven Coordinates

See [The Central Repository](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22de.h2b.java.lib.pa-toolbox%22%20OR%20g%3A%22de.h2b.scala.lib.pa-toolbox%22%20%20OR%20a%3A%22pa-toolbox%22). Choose the version you want (of course, the latest one is recommended) and look for "Dependency Information".

## Acknowledgement

This library originally has been developed by me during my former employment at [SOTA SOLUTIONS GmbH](http://sota-solutions.de/). I am grateful for their allowance to publish it under an open-source licence.

## License

PA-Toolbox - Predictive Analytics Java Toolbox 

Copyright 2014-2018 Hans-Hermann Bode

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Note that some submodules may have a different license due to external dependencies.
