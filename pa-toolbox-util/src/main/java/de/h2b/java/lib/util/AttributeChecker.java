/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A class that checks a set of attributes against a set of required and another
 * set of optional attributes.
 *
 * @param <A> the type of the attributes
 *
 * @author h2b
 *
 */
public class AttributeChecker<A> {
	
	private Set<A> specimen;
	
	private Set<A> required;
	
	private Set<A> optional;
	
	private Set<A> missing;
	
	private Set<A> redundant;

	/**
	 * Constructs a new attribute checker. For all arguments, {@code null} 
	 * values are allowed and are equivalent to empty sets.
	 *
	 * @param specimen set of attributes under test
	 * @param required set of required attributes
	 * @param optional set of optional attributes
	 */
	public AttributeChecker(Set<A> specimen, Set<A> required, Set<A> optional) {
		super();
		this.specimen = instanciated(specimen);
		this.required = instanciated(required);
		this.optional = instanciated(optional);
		this.missing = new HashSet<A>();
		this.redundant = new HashSet<A>();
		check();
	}
	
	private Set<A> instanciated(Set<A> set) {
		Set<A> result = set;
		if (result==null) {
			result = Collections.emptySet();
		}
		return result;
	}

	private void check() {
		for (A attribute : required) {
			if (!specimen.contains(attribute)) {
				missing.add(attribute);
			}
		}
		for (A attribute : specimen) {
			if (!(optional.contains(attribute) || required.contains(attribute))) {
				redundant.add(attribute);
			}
		}
	}
	
	/**
	 * Returns if the specimen has neither missing nor redundant attributes. 
	 *
	 * @return
	 */
	public boolean isConsistent() {
		return missing.isEmpty() && redundant.isEmpty();
	}
	
	/**
	 * Returns if the specimen lacks of a required attribute.
	 *
	 * @return
	 */
	public boolean isDisturbed() {
		return !missing.isEmpty();
	}
	
	/**
	 * Returns if the specimen has attributes that are neither required nor
	 * optional.
	 *
	 * @return
	 */
	public boolean isContaminated() {
		return !redundant.isEmpty();
	}

	/**
	 * Gets {@code missing}.
	 *
	 * @return the set of non-present required attributes
	 */
	public Set<A> getMissing() {
		return missing;
	}

	/**
	 * Gets {@code redundant}.
	 *
	 * @return the set of neither required nor optional attributes
	 */
	public Set<A> getRedundant() {
		return redundant;
	}

}
