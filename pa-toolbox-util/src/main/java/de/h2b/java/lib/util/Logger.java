/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.Date;

/**
 * A class implementing a simple static logging utility.
 * 
 * @author h2b
 *
 */
public class Logger {

	/**
	 * Enumeration of logging levels provided.
	 * 
	 * The higher the order of a value in the sequence, the more messages will 
	 * be logged. In fact, the messages produced by a certain level are a
	 * superset of the messages from the lower levels, respectively.
	 * 
	 * @author h2b
	 *
	 */
	public enum Level {OFF, FATAL, WARNING, INFO, DEBUG}; 
	
	/**
	 * The current {@code Level} used for logging messages.
	 */
	private static Level currentLevel = Level.DEBUG;
	
	/**
	 * Gets the current {@code Level} used for logging messages.
	 * 
	 * @return the currentLevel
	 */
	public static Level getCurrentLevel() {
		return currentLevel;
	}

	/**
	 * Sets the current {@code Level} to be used for logging messages.
	 * 
	 * @param currentLevel the currentLevel to set
	 */
	public static void setCurrentLevel(Level currentLevel) {
		Logger.currentLevel = currentLevel;
	}

	/**
	 * Logs a message provided that the level specified is lower or equal to
	 * the current level.
	 * 
	 * @param level
	 * @param msg
	 */
	public static void log (Level level, String msg) {
		if (Logger.currentLevel.compareTo(level)>=0) {
			System.out.println("+++ " + level + " " + new Date() + ": " + msg);
		}
	}

	/**
	 * Convenience method logging a message on {@code Level.FATAL}.
	 * 
	 * @param msg
	 */
	public static void fatal (String msg) {
		Logger.log(Level.FATAL, msg);
	}
	
	/**
	 * Convenience method logging a message on {@code Level.WARNING}.
	 * 
	 * @param msg
	 */
	public static void warn (String msg) {
		Logger.log(Level.WARNING, msg);
	}
	
	/**
	 * Convenience method logging a message on {@code Level.INFO}.
	 * 
	 * @param msg
	 */
	public static void info (String msg) {
		Logger.log(Level.INFO, msg);
	}
	
	/**
	 * Convenience method logging a message on {@code Level.DEBUG}.
	 * 
	 * @param msg
	 */
	public static void debug (String msg) {
		Logger.log(Level.DEBUG, msg);
	}
}
