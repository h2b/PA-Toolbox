/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Provides mapping of statistical values with accumulating functionality.
 * 
 * @author h2b
 *
 */
public class Statistics {
	
	private final Map<String, Long> statisticsMap = new LinkedHashMap<String, Long>();
	
	/**
	 * Adds the given value to the statistics entry with the specified name, if
	 * it already exists; puts a new entry into this statistics otherwise.
	 * The order by which new entries are added is preserved.
	 * 
	 * @param name
	 * @param value
	 * @return the previous value of this entry or {@code null} if none exists
	 */
	public Long accumulate(String name, long value) {
		Long oldval  = statisticsMap.get(name);
		statisticsMap.put(name, oldval==null? value: oldval+value);
		return oldval;
	}
	
	/**
	 * Accumulates all entries of the other statistics to this one.
	 * 
	 * @param other
	 */
	public void accumulateAll(Statistics other) {
		for (String name : other.nameSet()) {
			accumulate(name, other.get(name));
		}
	}
	
	/**
	 * @param name
	 * @return the value of the entry with the given name or 
	 *         {@code null} if none exists
	 */
	public Long get(String name) {
		return statisticsMap.get(name);
	}
	
	/**
	 * The resulting set is ordered by the insertion sequence of added entries.
	 * 
	 * @return a set of all names contained in this statistics
	 */
	public Set<String> nameSet() {
		return statisticsMap.keySet();
	}
	
	/**
	 * Removes all entries from this statistics. The statistics will be empty 
	 * after this call returns.
	 */
	public void clear() {
		statisticsMap.clear();
	}

	/**
	 * Returns a string representation of this statistics, suitable formatted 
	 * in the first place for console output.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int maxWidth = maxValueWidth();
		for (String name : nameSet()) {
			builder.append(String.format("%" + maxWidth + "d", get(name)));
			builder.append(' ');
			builder.append(name);
			builder.append('\n');
		}
		return builder.toString();
	}

	private int maxValueWidth() {
		int max = Integer.MIN_VALUE;
		for (Long value : statisticsMap.values()) {
			int len = value.toString().length();
			if (len>max) {
				max = len;
			}
		}
		return max;
	}

}
