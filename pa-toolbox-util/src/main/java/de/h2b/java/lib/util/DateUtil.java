/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.Calendar;

/**
 * Provides utilities for handling time values.
 * 
 * @author h2b
 *
 */
public class DateUtil {
	
	/**
	 * Maps weekdays to the {@code Calendar.DAY_OF_WEEK} field and long and 
	 * short names, respectively.
	 */
	public enum Weekday {
		//Implementation note: strings should be localized
		MONDAY (Calendar.MONDAY, "Montag", "Mo"),
		TUESDAY (Calendar.TUESDAY, "Dienstag", "Di"),
		WEDNESDAY (Calendar.WEDNESDAY, "Mittwoch", "Mi"),
		THURSDAY (Calendar.THURSDAY, "Donnerstag", "Do"),
		FRIDAY (Calendar.FRIDAY, "Freitag", "Fr"),
		SATURDAY (Calendar.SATURDAY, "Sonnabend", "Sa"),
		SUNDAY ( Calendar.SUNDAY, "Sonntag", "So");
		
		private final String longName, shortName;
		
		private final int code;

		private Weekday(int code, String longName, String shortName) {
			this.code = code;
			this.longName = longName;
			this.shortName = shortName;
		}

		@Override
		public String toString() {
			return shortName;
		}

		/**
		 * @return code corresponding to the {@code Calendar.DAY_OF_WEEK} field
		 */
		public int getCode() {
			return code;
		}

		/**
		 * @return
		 */
		public String getLongName() {
			return longName;
		}

		/**
		 * @return
		 */
		public String getShortName() {
			return shortName;
		}
	}
	
	/**
	 * Rounds the given calendar object to the nearest 15 minutes point in time.
	 * 
	 * @param cal
	 */
	//Implementation note: there should be a more general version of this method
	public static void roundMinutesToQuarter(Calendar cal) {
		final int minutes = cal.get(Calendar.MINUTE);
		final int mod15 = minutes % 15;
		cal.add(Calendar.MINUTE, mod15<8? -mod15: 15-mod15);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * Converts a time given in milliseconds to a convenient string 
	 * representation.
	 * 
	 * @param ms millseconds (non-negative value)
	 * @return
	 * @throws IllegalArgumentException if ms is negative
	 */
	public static String toString(long ms) throws IllegalArgumentException {
		if (ms<0) {
			throw new IllegalArgumentException("negative value for milliseconds: " + ms);
		}
		StringBuilder builder = new StringBuilder();
		final long days = ms / 86400000; ms %= 86400000;
		final long hours = ms / 3600000; ms %= 3600000;
		final long minutes = ms / 60000; ms %= 60000;
		final long seconds = ms / 1000; ms %= 1000;
		if (days>0) {
			builder.append(days);
			builder.append("d");
		}
		if (hours>0) {
			builder.append(hours);
			builder.append("h");
		}
		if (minutes>0) {
			builder.append(minutes);
			builder.append("m");
		}
		if (seconds>0) {
			builder.append(seconds);
			builder.append("s");
		}
		if (ms>0 || builder.length()==0) {
			builder.append(ms);
			builder.append("ms");
		}
		return builder.toString();
	}

}
