/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides static methods to correct and complete entries of a data list.
 * 
 * @author h2b
 *
 */
public class DataCleaner {
	
	private static final double LEFT_BOUNDARY = 0.0;
	
	private static final double RIGHT_BOUNDARY = 0.0;
	
	private final List<Double> values;

	private DataCleaner(List<Double> values) {
		this.values = values;
	}
	
	/**
	 * Returns a copy of the original list with all values considered to be 
	 * erroneously equal to zero replaced by {@code Double.NaN}.
	 * <p/>
	 * A value is considered to be erroneously equal to zero if on both sides
	 * the adjacent list entries have an absolute value greater than or equal to
	 * the specified threshold. The adjacent list entry is defined as the next
	 * entry that is not equal to {@code Double.NaN}.
	 * 
	 * @param values
	 * @param threshold
	 * @return the copy or {@code null} if the original list is {@code null}
	 */
	public static List<Double> zeroesAnnihilation(List<Double> values, double threshold) {
		return new DataCleaner(values).zeroesAnnihilated(threshold);
	}
	
	/**
	 * Returns a copy of the original list with all missing values replaced by
	 * a linear interpolation of the adjacent list entries. A value is defined
	 * as missing if it is equal to {@code Double.NaN}.
	 * <p/>
	 * The boundary conditions are that a value of {@value #LEFT_BOUNDARY}
	 * is assumed before the first element of the list and a value of
	 * {@value #RIGHT_BOUNDARY} after the last one.
	 * 
	 * @param values
	 * @return the copy or {@code null} if the original list is {@code null}
	 */
	public static List<Double> linearInterpolation(List<Double> values) {
		return new DataCleaner(values).linearInterpolated();
	}

	private List<Double> zeroesAnnihilated(double threshold) {
		if (values==null) {
			return null;
		}
		List<Double> result = new ArrayList<Double>(values.size());
		for (int i = 0; i < values.size(); i++) {
			double val = values.get(i);
			if (Double.compare(val, 0.0)==0 && toBeAnnihilated(i, threshold)) {
				val = Double.NaN;
			}
			result.add(val);
		}
		return result;
	}

	private boolean toBeAnnihilated(int i, double threshold) {
		double adjacent = 0;
		int j;
		//search next valid entry on the left
		j = i-1;
		while (j>=0 && Double.compare(adjacent=values.get(j), Double.NaN)==0) {
			j--;
		}
		if (j<0 || Double.compare(Math.abs(adjacent), threshold)<0) {
			return false; //no valid entry or below threshold on the left
		}
		//search next valid entry on the right
		j = i+1;
		while (j<values.size() && Double.compare(adjacent=values.get(j), Double.NaN)==0) {
			j++;
		}
		if (j>=values.size() || Double.compare(Math.abs(adjacent), threshold)<0) {
			return false; //no valid entry or below threshold on the right
		}
		//valid entry above threshold on both sides
		return true;
	}
	
	private List<Double> linearInterpolated() {
		if (values==null) {
			return null;
		}
		List<Double> result = new ArrayList<Double>(values.size());
		double left = LEFT_BOUNDARY;
		for (int i = 0; i < values.size(); i++) {
			double val = values.get(i);
			if (isMissing(val)) {
				int rightIdx = nextRight(i);
				double right = rightIdx<values.size()? values.get(rightIdx): RIGHT_BOUNDARY;
				val = left+(right-left)/(rightIdx-i+1);
			}
			result.add(val);
			left = val;
		}
		return result;
	}

	private int nextRight(int i) {
		int j = i+1;
		while (j<values.size() && isMissing(values.get(j))) {
			j++;
		}
		return j;
	}

	private boolean isMissing(double val) {
		return Double.compare(val, Double.NaN)==0;
	}

}
