/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Represents an immutable calendar day.
 * <p/>
 * All arguments entered into constructor or method calls must be non-{@code null}.
 * <p/>
 * Note that all information related to time units smaller than a day will be 
 * stripped off on constructing. Thus, e.g. in the sequence
 * {@code 
 * 		Date date = new Date();
 * 		Day day = new Day(date);
 * 		boolean equal = date.equals(day.toDate());
 * }
 * the resulting {@code equal} might not be {@code true}.
 * 
 * @author h2b
 *
 */
public final class Day {
	
	private static final DateFormat DEFAULT_DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	private final long ms;

	public Day(long ms) {
		this.ms = stripTime(ms);
	}
	
	private static long stripTime(long ms) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ms);
		stripTime(cal);
		return cal.getTimeInMillis();
	}

	private static void stripTime(Calendar cal) {
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Today.
	 */
	public Day() {
		this(System.currentTimeMillis());
	}
	
	/**
	 * @param year 1..31
	 * @param month 1..12
	 * @param date
	 */
	public Day(int year, int month, int date) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, date);
		stripTime(cal);
		ms = cal.getTimeInMillis();
	}
	
	/**
	 * String parsed by format.
	 * 
	 * @param string
	 * @param format
	 * @throws ParseException
	 */
	public Day(String string, DateFormat format) throws ParseException {
		this(format.parse(string));
	}

	public Day(Date date) {
		this(date.getTime());
	}
	
	public Day(Calendar calendar) {
		this(calendar.getTimeInMillis());
	}
	
	public boolean before(Day when) {
		return this.ms<when.ms;
	}
	
	public boolean after(Day when) {
		return this.ms>when.ms;
	}
	
	public int compareTo(Day other) {
		return Long.signum(this.ms-other.ms);
	}

	@Override
	public int hashCode() {
		return (int) (ms^(ms>>>32));
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Day) {
			Day other = (Day) obj;
			return this.ms==other.ms;
		}
		return false;
	}

	@Override
	public String toString() {
		return toString(DEFAULT_DATEFORMAT);
	}
	
	public String toString(DateFormat format) {
		return format.format(new Date(ms));
	}
	
	public Date toDate() {
		return new Date(ms);
	}
	
	public Calendar toCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ms);
		return cal;
	}
	
	public long toMs() {
		return ms;
	}
	
	public Day plus(int numberOfDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ms);
		cal.add(Calendar.DAY_OF_YEAR, numberOfDays);
		return new Day(cal);
	}
	
	public Day minus(int numberOfDays) {
		return plus(-numberOfDays);
	}
	
	public long diff(Day other) {
		if (this.after(other)) {
			return days(other, this);
		} else if (this.before(other)) {
			return -days(this, other);
		} else {
			return 0;
		}
	}

	private static long days(Day from, Day to) {
		//this should take daylight savings into account
		long sum = 0;
		for (Day day = from.plus(1); day.compareTo(to)<=0; day=day.plus(1)) {
			sum++;
		}
		return sum;
	}
	
}
