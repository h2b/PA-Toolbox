/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Provides some generic utility methods for collections.
 * 
 * @author h2b
 *
 */
public class Collections {
	
	/**
	 * Returns the sub-collection of elements for which the predicate applies.
	 * 
	 * Returns {@code null} if the source collection or the predicate is
	 * {@code null}. 
	 * 
	 * @param collection
	 * @param predicate
	 * @return
	 */
	public static <T> Collection<T> filter (Collection<T> collection, 
			Predicate<T> predicate) {
		if (collection==null || predicate==null) {
			return null;
		}
		Collection<T> result = new ArrayList<T>();
		for (T elem : collection) {
			if (predicate.applies(elem)) {
				result.add(elem);
			}
		}
		return result;
	}
	
	/**
	 * Returns the first element for which the predicate applies.
	 * 
	 * Returns {@code null} if the source collection or the predicate is
	 * {@code null} or if there is no element for which the predicate applies. 
	 * 
	 * @param collection
	 * @param predicate
	 * @return 
	 * @return
	 */
	public static <T> T selectFirst (Collection<T> collection, 
			Predicate<T> predicate) {
		if (collection==null || predicate==null) {
			return null;
		}
		for (T elem : collection) {
			if (predicate.applies(elem)) {
				return elem;
			}
		}
		return null;
	}
	
	/**
	 * Returns {@code true} if the collection contains at least one element for 
	 * which the predicate applies, otherwise {@code fasle}.
	 * 
	 * Returns {@code false} if the source collection or the predicate is
	 * {@code null}. 
	 * 
	 * @param collection
	 * @param predicate
	 * @return
	 */
	public static <T> boolean contains (Collection<T> collection, Predicate<T> predicate) {
		if (collection==null || predicate==null) {
			return false;
		}
		for (T elem : collection) {
			if (predicate.applies(elem)) {
				return true;
			}
		}
		return false;
	}
}
