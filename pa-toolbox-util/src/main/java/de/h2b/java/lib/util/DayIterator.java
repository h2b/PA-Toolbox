/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.util.Iterator;

/**
 * @author h2b
 *
 */
public class DayIterator implements Iterable<Day> {
	
	private final Day from;
	private final Day until;
	private final int step;

	/**
	 * Constructs a new iterator running for each day in the interval 
	 * {@code [from, until)} (left bound inclusive, right bound exclusive).
	 * 
	 * @param from
	 * @param until
	 */
	public DayIterator(Day from, Day until) {
		this(from, until, 1);
	}

	/**
	 * Constructs a new iterator running by the specified step of days in the interval 
	 * {@code [from, until)} (left bound inclusive, right bound exclusive).
	 * 
	 * @param from
	 * @param until
	 * @param step
	 */
	public DayIterator(Day from, Day until, int step) {
		this.from = from;
		this.until = until;
		this.step = step;
	}

	/**
	 * Does not support remove operation.
	 */
	@Override
	public Iterator<Day> iterator() {
		return new Iterator<Day>() {
			
			private Day current = from;
			
			/**
			 * Not supported.
			 * 
			 * @throws UnsupportedOperationException if called
			 */
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Day next() {
				Day next = current;
				current = current.plus(step);
				return next;
			}
			
			@Override
			public boolean hasNext() {
				return current.before(until);
			}
		};
	}

}
