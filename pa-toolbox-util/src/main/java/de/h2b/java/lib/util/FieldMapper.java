/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Maps strings to fields of classes and provides methods to handle these 
 * fields. 
 * 
 * @author h2b
 *
 */
public class FieldMapper {
	
	private Map<String, Field> fieldMap;
	
	/**
	 * 
	 */
	public FieldMapper() {
		super();
		fieldMap = new HashMap<String, Field>();
	}
	
	/**
	 * @param key
	 * @param field
	 */
	public void put(String key, Field field) {
		field.setAccessible(true);
		fieldMap.put(key, field);
	}
	
	/**
	 * @param key
	 * @return a new object of the declaring class of the field mapped by the key
	 * @throws IllegalArgumentException if key wasn't put before
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Object createObjectForKey(String key) 
			throws IllegalArgumentException, InstantiationException, IllegalAccessException {
		return getDeclaringClass(key).newInstance();
	}
	
	/**
	 * @param key
	 * @return the field mapped by the key
	 * @throws IllegalArgumentException if key wasn't put before
	 */
	public Field get(String key) throws IllegalArgumentException {
		Field field = fieldMap.get(key);
		if (field==null) {
			throw new IllegalArgumentException("No mapping for key: " + key);
		}
		return field;
	}

	/**
	 * @param key
	 * @param obj
	 * @param value the value of the field of the obj mapped by the key
	 * @throws IllegalArgumentException if key wasn't put before
	 * @throws IllegalAccessException
	 */
	public void setFieldValue(String key, Object obj, Object value) 
			throws IllegalArgumentException, IllegalAccessException {
		get(key).set(obj, value);
	}
	
	/**
	 * @param key
	 * @param obj
	 * @return the value of the field of the obj mapped by the key
	 * @throws IllegalArgumentException if key wasn't put before
	 * @throws IllegalAccessException
	 */
	public Object getFieldValue(String key, Object obj) 
			throws IllegalArgumentException, IllegalAccessException {
		return get(key).get(obj);
	}
	
	/**
	 * 
	 * @param key
	 * @return the declaring class of the field mapped by the key
	 */
	public Class<?> getDeclaringClass(String key) {
		return get(key).getDeclaringClass();
	} 

}
