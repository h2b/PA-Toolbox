/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public class FieldMapperTest {
	
	public static class TestClass {
		private int i;
		private String str;
	}
	
	private static final String TEST_KEY_I = "int field";
	
	private static final String TEST_KEY_STR = "string field";
	
	private static FieldMapper testMapper;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		testMapper = new FieldMapper();
		testMapper.put(TEST_KEY_I, TestClass.class.getDeclaredField("i"));
		testMapper.put(TEST_KEY_STR, TestClass.class.getDeclaredField("str"));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#createObjectForKey(java.lang.String)}.
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testCreateObjectForKey() throws IllegalArgumentException, InstantiationException, IllegalAccessException {
		Object actual = testMapper.createObjectForKey(TEST_KEY_I);
		assertTrue("actual is null", actual!=null);
		assertTrue("actual is no instance of test class", actual instanceof TestClass);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#setFieldValue(java.lang.String, java.lang.Object, java.lang.Object)}.
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testSetFieldValue() throws IllegalArgumentException, IllegalAccessException {
		TestClass testObj = new TestClass();
		testMapper.setFieldValue(TEST_KEY_I, testObj, 1);
		testMapper.setFieldValue(TEST_KEY_STR, testObj, "test");
		assertEquals(1, testObj.i);
		assertEquals("test", testObj.str);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#setFieldValue(java.lang.String, java.lang.Object, java.lang.Object)}.
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSetFieldValueWrongType() throws IllegalAccessException {
		testMapper.setFieldValue(TEST_KEY_I, new TestClass(), "test");
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#getFieldValue(java.lang.String, java.lang.Object)}.
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testGetFieldValue() throws IllegalArgumentException, IllegalAccessException {
		TestClass testObj = new TestClass();
		testObj.i = 1;
		testObj.str = "test";
		assertEquals(1, testMapper.getFieldValue(TEST_KEY_I, testObj));
		assertEquals("test", (String) testMapper.getFieldValue(TEST_KEY_STR, testObj));
	}
	
	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#getClassForKey(String))}.
	 */
	@Test
	public void testGetDeclaringClass() {
		assertEquals(TestClass.class, testMapper.getDeclaringClass(TEST_KEY_I));
		assertEquals(TestClass.class, testMapper.getDeclaringClass(TEST_KEY_STR));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.FieldMapper#get(String))}.
	 * @throws IllegalArgumentException 
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 */
	@Test
	public void testGet() throws IllegalArgumentException, NoSuchFieldException, SecurityException {
		Field expectedI = TestClass.class.getDeclaredField("i");
		Field expextedStr = TestClass.class.getDeclaredField("str");
		assertEquals(expectedI, testMapper.get(TEST_KEY_I));
		assertEquals(expextedStr, testMapper.get(TEST_KEY_STR));
	}
	
}
