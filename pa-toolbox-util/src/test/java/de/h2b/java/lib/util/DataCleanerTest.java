/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static java.lang.Double.NaN;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public final class DataCleanerTest {
	
	private static final List<Double> TEST_DATA_1 = Arrays.asList(1.0,2.0,3.0,4.0,5.0);
	
	private static final List<Double> TEST_DATA_2 = Arrays.asList(1.0,NaN,3.0,NaN,5.0);

	private static final List<Double> TEST_DATA_3 = Arrays.asList(1.0,0.0,3.0,0.0,5.0);

	/**
	 * Test method for {@link de.h2b.java.lib.util.DataCleaner#zeroesAnnihilation(java.util.List, double)}.
	 */
	@Test
	public void testZeroesAnnihilated() {
		assertEquals(TEST_DATA_2, DataCleaner.zeroesAnnihilation(TEST_DATA_3, 1.0));
		assertEquals(TEST_DATA_3, DataCleaner.zeroesAnnihilation(TEST_DATA_3, 10.0));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DataCleaner#linearInterpolation(java.util.List)}.
	 */
	@Test
	public void testLinearInterpolated() {
		assertEquals(TEST_DATA_1, DataCleaner.linearInterpolation(TEST_DATA_2));
		assertEquals(TEST_DATA_1, DataCleaner.linearInterpolation(TEST_DATA_1));
	}

}
