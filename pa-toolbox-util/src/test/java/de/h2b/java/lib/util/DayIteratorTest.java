/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public final class DayIteratorTest {

	/**
	 * Test method for {@link de.h2b.java.lib.util.DayIterator#DayIterator(de.h2b.java.lib.util.Day, de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testDayIteratorDayDay() {
		Day from = new Day(2015, 6, 1);
		Day until = new Day(2015, 6, 8);
		assertEquals(7, count(from, until));
		assertEquals(1, count(from, from.plus(1)));
		assertEquals(0, count(from, from));
		assertEquals(0, count(until, from));
	}

	private int count(Day from, Day until) {
		DayIterator iterator = new DayIterator(from, until);
		int sum = 0;
		for (@SuppressWarnings("unused") Day day : iterator) {
			sum++;
		}
		return sum;
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DayIterator#DayIterator(de.h2b.java.lib.util.Day, de.h2b.java.lib.util.Day, int)}.
	 */
	@Test
	public void testDayIteratorDayDayInt() {
		Day from = new Day(2015, 6, 1);
		Day until = new Day(2015, 6, 9);
		assertEquals(4, count(from, until, 2));
		assertEquals(3, count(from, until, 3));
		assertEquals(1, count(from, until, 8));
	}

	private int count(Day from, Day until, int step) {
		DayIterator iterator = new DayIterator(from, until, step);
		int sum = 0;
		for (@SuppressWarnings("unused") Day day : iterator) {
			sum++;
		}
		return sum;
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DayIterator#iterator()}.
	 */
	@Test
	public void testIteratorFullYear() {
		Day day1 = new Day(2014, 7, 1);
		Day day2 = new Day(2015, 7, 1);
		assertEquals(365, count(day1, day2));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DayIterator#iterator()}.
	 */
	@Test
	public void testIteratorClockChange1() {
		Day day1 = new Day(2014, 10, 21);
		Day day2 = new Day(2014, 11, 5);
		assertEquals(15, count(day1, day2));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DayIterator#iterator()}.
	 */
	@Test
	public void testIteratorClockChange2() {
		Day day1 = new Day(2015, 3, 21);
		Day day2 = new Day(2015, 4, 5);
		assertEquals(15, count(day1, day2));
	}

}
