/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public final class DayTest {

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#Day(int, int, int)}.
	 */
	@Test
	public void testDayIntIntInt() {
		Day day = new Day(2015, 6, 8);
		assertEquals("2015-06-08", day.toString());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#Day(int, int, int)}.
	 */
	@Test
	public void testDayIntIntIntOutOfRange() {
		new Day(2015, 66, 88);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#Day(java.lang.String, java.text.DateFormat)}.
	 * @throws ParseException 
	 */
	@Test
	public void testDayStringDateFormat() throws ParseException {
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Day day = new Day("14.10.1999", format);
		assertEquals("1999-10-14", day.toString());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#Day(java.util.Date)}.
	 */
	@Test
	public void testDayDate() {
		Date date = new Date();
		Day day = new Day(date);
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		assertEquals(format.format(date), day.toString(format));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#Day(java.util.Calendar)}.
	 */
	@Test
	public void testDayCalendar() {
		Calendar cal = Calendar.getInstance();
		Day day = new Day(cal);
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		assertEquals(format.format(cal.getTime()), day.toString(format));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#before(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testBefore() {
		Day day1 = new Day(2000, 1, 15);
		Day day2 = new Day(2000, 1, 16);
		assertTrue(day1.before(day2));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#after(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testAfter() {
		Day day1 = new Day(2000, 1, 15);
		Day day2 = new Day(2000, 1, 16);
		assertTrue(day2.after(day1));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#compareTo(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testCompareTo() {
		Day day1 = new Day(2000, 1, 15);
		Day day2 = new Day(2000, 1, 16);
		assertTrue(day1.compareTo(day2)<0);
		assertTrue(day2.compareTo(day1)>0);
		assertTrue(day1.compareTo(day1)==0);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		Day day1 = new Day(2000, 1, 15);
		Day day2 = new Day(2000, 1, 15);
		Day day3 = new Day(2000, 1, 16);
		assertTrue(day1.equals(day2));
		assertFalse(day1.equals(day3));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#toString()}.
	 */
	@Test
	public void testToString() {
		Day day = new Day(1957, 2, 17);
		assertEquals("1957-02-17", day.toString());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#toDate()}.
	 * @throws ParseException 
	 */
	@Test
	public void testToDate() throws ParseException {
		Day day = new Day(1957, 2, 17);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse("1957-02-17");
		assertEquals(date, day.toDate());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#toCalendar()}.
	 */
	@Test
	public void testToCalendar() {
		Day day = new Day(1957, 2, 17);
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(1957, Calendar.FEBRUARY, 17);
		assertEquals(cal, day.toCalendar());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#plus(int)}.
	 */
	@Test
	public void testPlus() {
		Day day = new Day(2015, 2, 15);
		assertEquals(new Day(2015, 3, 17), day.plus(30));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#minus(int)}.
	 */
	@Test
	public void testMinus() {
		Day day = new Day(2010, 1, 10);
		assertEquals(new Day(2009, 12, 21), day.minus(20));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#diff(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testDiff() {
		Day day1 = new Day(2014, 7, 1);
		Day day2 = new Day(2015, 7, 1);
		Day day3 = new Day(2015, 7, 2);
		assertEquals(365, day2.diff(day1));
		assertEquals(-1, day2.diff(day3));
		assertEquals(0, day1.diff(day1));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#diff(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testDiffClockChange1() {
		Day day1 = new Day(2014, 10, 21);
		Day day2 = new Day(2014, 11, 5);
		assertEquals(15, day2.diff(day1));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Day#diff(de.h2b.java.lib.util.Day)}.
	 */
	@Test
	public void testDiffClockChange2() {
		Day day1 = new Day(2015, 3, 21);
		Day day2 = new Day(2015, 4, 5);
		assertEquals(15, day2.diff(day1));
	}

}
