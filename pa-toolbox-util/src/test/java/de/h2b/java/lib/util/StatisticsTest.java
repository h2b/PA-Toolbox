/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public final class StatisticsTest {
	
	private static final Statistics TEST_STAT1 = new Statistics();
	
	private static final Statistics TEST_STAT2 = new Statistics();
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		TEST_STAT1.accumulate("one", 1);
		TEST_STAT1.accumulate("two", 2);
		TEST_STAT2.accumulate("two", 2);
		TEST_STAT2.accumulate("three", 30);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		TEST_STAT1.clear();
		TEST_STAT2.clear();
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.Statistics#accumulateAll(de.h2b.java.lib.util.Statistics)}.
	 */
	@Test
	public void testAccumulateAll() {
		TEST_STAT1.accumulateAll(TEST_STAT2);
		assertEquals((long)1, (long)TEST_STAT1.get("one"));
		assertEquals((long)4, (long)TEST_STAT1.get("two"));
		assertEquals((long)30, (long)TEST_STAT1.get("three"));
		assertNull(TEST_STAT1.get("null"));
	}
	
	/**
	 * Test method for {@link de.h2b.java.lib.util.Statistics#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("1 one\n2 two\n", TEST_STAT1.toString());
		assertEquals(" 2 two\n30 three\n", TEST_STAT2.toString());
	}

}
