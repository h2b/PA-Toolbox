/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public final class DateUtilTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DateUtil#roundMinutesToQuarter(java.util.Calendar)}.
	 */
	@Test
	public void testRoundMinutesToQuarter() {
		Calendar act1 = new GregorianCalendar(2015, 1, 27, 14, 40, 35);
		Calendar exp1 = new GregorianCalendar(2015, 1, 27, 14, 45, 0);
		DateUtil.roundMinutesToQuarter(act1);
		Calendar act2 = new GregorianCalendar(1969, 6, 20, 21, 17, 12);
		Calendar exp2 = new GregorianCalendar(1969, 6, 20, 21, 15, 0);
		DateUtil.roundMinutesToQuarter(act2);
		assertEquals(exp1, act1);
		assertEquals(exp2, act2);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.DateUtil#toString(long)}.
	 */
	@Test
	public void testToStringLong() {
		assertEquals("0ms", DateUtil.toString(0));
		assertEquals("15ms", DateUtil.toString(15));
		assertEquals("25s", DateUtil.toString(25000));
		assertEquals("1h", DateUtil.toString(3600000));
		assertEquals("1d12h30m1ms", DateUtil.toString(86400000+12*3600000+30*60000+1));
	}

}
