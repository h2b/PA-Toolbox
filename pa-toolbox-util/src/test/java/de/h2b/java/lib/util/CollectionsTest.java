/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public class CollectionsTest {
	
	/**
	 * Array for testing.
	 */
	private static final Integer [] TEST_ARRAY = {1, 2, 3, 4, 5};
	
	/**
	 * Sub-array of even numbers in {@link #TEST_ARRAY}. 
	 */
	private static final Integer [] EVEN_ARRAY = {2, 4};
	
	/**
	 * Sub-array of odd numbers in {@link #TEST_ARRAY}. 
	 */
	private static final Integer [] ODD_ARRAY = {1, 3, 5};
	
	/**
	 * Implements predicate "number is even".
	 */
	private static final Predicate<Integer> IS_EVEN = new Predicate<Integer>() {
		@Override
		public boolean applies(Integer t) {
			return t%2==0;
		}
	};
	
	/**
	 * Verifies that predicate filters the correct sub-array.
	 */
	@Test
	public void testFilter() {
		ArrayList<Integer> testList = new ArrayList<Integer>(Arrays.asList(TEST_ARRAY));
		Collection<Integer> actual = Collections.filter(testList, IS_EVEN);
		assertTrue(EVEN_ARRAY.length==actual.size() && 
				actual.contains(EVEN_ARRAY[0]) && actual.contains(EVEN_ARRAY[1]));
	}

	/**
	 * Verifies {@code null} behavior of filtering.
	 */
	@Test
	public void testFilterNull() {
		Collection<Integer> actual1 = Collections.filter(null, IS_EVEN);
		Collection<Integer> actual2 = Collections.filter(Arrays.asList(TEST_ARRAY), null);
		Collection<Integer> actual3 = Collections.filter(null, null);
		assertTrue(actual1==null && actual2==null && actual3==null);
	}
	
	/**
	 * Verifies that predicate selects the correct first element.
	 */
	@Test
	public void testSelectFirst() {
		ArrayList<Integer> testList = new ArrayList<Integer>(Arrays.asList(TEST_ARRAY));
		Integer actual = Collections.selectFirst(testList, IS_EVEN);
		assertTrue(actual==2);
	}

	/**
	 * Verifies {@code null} behavior of selecting.
	 */
	@Test
	public void testSelectFirstNull() {
		ArrayList<Integer> testList = new ArrayList<Integer>(Arrays.asList(ODD_ARRAY));
		Integer actual0 = Collections.selectFirst(testList, IS_EVEN);
		Integer actual1 = Collections.selectFirst(null, IS_EVEN);
		Integer actual2 = Collections.selectFirst(Arrays.asList(TEST_ARRAY), null);
		Integer actual3 = Collections.selectFirst(null, null);
		assertTrue(actual0==null && actual1==null && actual2==null && actual3==null);
	}
	
	/**
	 * Verifies that predicate determines correct containment.
	 */
	@Test
	public void testContains() {
		ArrayList<Integer> testList = new ArrayList<Integer>(Arrays.asList(TEST_ARRAY));
		ArrayList<Integer> oddList = new ArrayList<Integer>(Arrays.asList(ODD_ARRAY));
		assertTrue(Collections.contains(testList, IS_EVEN) && !Collections.contains(oddList, IS_EVEN));
	}
	
	/**
	 * Verifies {@code null} behavior of containment.
	 */
	@Test
	public void testContainsNull() {
		boolean actual1 = Collections.contains(null, IS_EVEN);
		boolean actual2 = Collections.contains(Arrays.asList(TEST_ARRAY), null);
		boolean actual3 = Collections.contains(null, null);
		assertFalse(actual1 || actual2 || actual3);
	}
}
