/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public class AttributeCheckerTest {
	
	private static final Set<String> REQUIRED = 
			new HashSet<String>(Arrays.asList("A", "B", "C"));
	
	private static final Set<String> OPTIONAL =
			new HashSet<String>(Arrays.asList("X", "Y", "Z"));
	
	private static final Set<String> CONSISTENT =
			new HashSet<String>(Arrays.asList("B", "X", "C", "A"));
	
	private static final Set<String> DISTURBED =
			new HashSet<String>(Arrays.asList("A", "B", "Z"));
	
	private static final Set<String> DISTURBED_MISSING =
			new HashSet<String>(Arrays.asList("C"));
	
	private static final Set<String> CONTAMINATED =
			new HashSet<String>(Arrays.asList("A", "u", "B", "v", "C", "X"));
	
	private static final Set<String> CONTAMINATED_REDUNDANT =
			new HashSet<String>(Arrays.asList("u", "v"));
	
	private static final Set<String> DISTURBED_CONTAMINATED =
			new HashSet<String>(Arrays.asList("u", "C", "Y"));
	
	private static final Set<String> DISTURBED_CONTAMINATED_MISSING =
			new HashSet<String>(Arrays.asList("A", "B"));

	private static final Set<String> DISTURBED_CONTAMINATED_REDUNDANT =
			new HashSet<String>(Arrays.asList("u"));
	
	private static final Set<String> NULL = null;
	
	private static final Set<String> NULL_MISSING = REQUIRED;

	private AttributeChecker<String> checkerConsistent;
	
	private AttributeChecker<String> checkerDisturbed;
	
	private AttributeChecker<String> checkerContaminated;
	
	private AttributeChecker<String> checkerDisturbedContaminated;
	
	private AttributeChecker<String> checkerNull;

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		checkerConsistent = new AttributeChecker<String>(CONSISTENT, REQUIRED, OPTIONAL);
		checkerDisturbed = new AttributeChecker<String>(DISTURBED, REQUIRED, OPTIONAL);
		checkerContaminated = new AttributeChecker<String>(CONTAMINATED, REQUIRED, OPTIONAL);
		checkerDisturbedContaminated = new AttributeChecker<String>(DISTURBED_CONTAMINATED, REQUIRED, OPTIONAL);
		checkerNull = new AttributeChecker<String>(NULL, REQUIRED, OPTIONAL);
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.AttributeChecker#isConsistent()}.
	 */
	@Test
	public void testIsConsistent() {
		assertTrue("checkerConsistent inconsistent", checkerConsistent.isConsistent());
		assertTrue("checkerDisturbed consistent", !checkerDisturbed.isConsistent());
		assertTrue("checkerContaminated consistent", !checkerContaminated.isConsistent());
		assertTrue("checkerDisturbedContaminated consistent", !checkerDisturbedContaminated.isConsistent());
		assertTrue("checkerNull consistent", !checkerNull.isConsistent());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.AttributeChecker#isDisturbed()}.
	 */
	@Test
	public void testIsDisturbed() {
		assertTrue("checkerConsistent disturbed", !checkerConsistent.isDisturbed());
		assertTrue("checkerDisturbed not disturbed", checkerDisturbed.isDisturbed());
		assertTrue("checkerContaminated disturbed", !checkerContaminated.isDisturbed());
		assertTrue("checkerDisturbedContaminated not disturbed", checkerDisturbedContaminated.isDisturbed());
		assertTrue("checkerNull not disturbed", checkerNull.isDisturbed());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.AttributeChecker#isContaminated()}.
	 */
	@Test
	public void testIsContaminated() {
		assertTrue("checkerConsistent contaminated", !checkerConsistent.isContaminated());
		assertTrue("checkerDisturbed contaminated", !checkerDisturbed.isContaminated());
		assertTrue("checkerContaminated not contaminated", checkerContaminated.isContaminated());
		assertTrue("checkerDisturbedContaminated not contaminated", checkerDisturbedContaminated.isContaminated());
		assertTrue("checkerNull contaminated", !checkerNull.isContaminated());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.AttributeChecker#getMissing()}.
	 */
	@Test
	public void testGetMissing() {
		assertTrue("checkerDisturbed false missing", DISTURBED_MISSING.equals(checkerDisturbed.getMissing()));
		assertTrue("checkerDisturbedContaminated false missing", DISTURBED_CONTAMINATED_MISSING.equals(checkerDisturbedContaminated.getMissing()));
		assertTrue("checkerNull false missing", NULL_MISSING.equals(checkerNull.getMissing()));
	}

	/**
	 * Test method for {@link de.h2b.java.lib.util.AttributeChecker#getRedundant()}.
	 */
	@Test
	public void testGetRedundant() {
		assertTrue("checkerContaminated false redundant", CONTAMINATED_REDUNDANT.equals(checkerContaminated.getRedundant()));
		assertTrue("checkerDisturbedContaminated false redundant", DISTURBED_CONTAMINATED_REDUNDANT.equals(checkerDisturbedContaminated.getRedundant()));
	}

}
