/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public class TableElementTest {
	
	private static final boolean VAL_BOOLEAN = true;
	
	private static final int VAL_INT = 123;
	
	private static final String VAL_STRING = "Hello world!";
	
	private static final Date VAL_DATE = new Date();
	
	private static final Calendar VAL_CALENDAR = Calendar.getInstance();
	
	private static final TableElement<Boolean> TEST_BOOLEAN = TableElement.of(VAL_BOOLEAN);
	
	private static final TableElement<Integer> TEST_INTEGER = TableElement.of(VAL_INT);
	
	private static final TableElement<String> TEST_STRING = TableElement.of(VAL_STRING);
	
	private static final TableElement<Date> TEST_DATE = TableElement.of(VAL_DATE);
	
	private static final TableElement<Calendar> TEST_CALENDAR = TableElement.of(VAL_CALENDAR);

	/**
	 * Test method for {@link de.h2b.java.lib.office.TableElement#get()}.
	 */
	@Test
	public void testValuesGet() {
		assertEquals(VAL_BOOLEAN, TEST_BOOLEAN.get());
		assertEquals(VAL_INT, (int)TEST_INTEGER.get());
		assertEquals(VAL_STRING, TEST_STRING.get());
		assertEquals(VAL_DATE, TEST_DATE.get());
		assertEquals(VAL_CALENDAR, TEST_CALENDAR.get());
	}

	/**
	 * Test method for {@link de.h2b.java.lib.office.TableElement#get()}.
	 */
	@Test
	public void testTypesGet() {
		assertEquals(Boolean.class, TEST_BOOLEAN.get().getClass());
		assertEquals(Integer.class, TEST_INTEGER.get().getClass());
		assertEquals(String.class, TEST_STRING.get().getClass());
		assertEquals(Date.class, TEST_DATE.get().getClass());
		assertTrue(TEST_CALENDAR.get() instanceof Calendar);
	}
	
	/**
	 * Tests building a list of different {@code TableElement} entries.
	 */
	@Test
	public void testList() {
		List<TableElement<?>> list = new ArrayList<TableElement<?>>();
		list.add(TEST_BOOLEAN);
		list.add(TEST_INTEGER);
		list.add(TEST_STRING);
		list.add(TEST_DATE);
		list.add(TEST_CALENDAR);
		assertEquals(VAL_BOOLEAN, list.get(0).get());
		assertEquals(VAL_INT, list.get(1).get());
		assertEquals(VAL_STRING, list.get(2).get());
		assertEquals(VAL_DATE, list.get(3).get());
		assertEquals(VAL_CALENDAR, list.get(4).get());
		assertEquals(Boolean.class, list.get(0).get().getClass());
		assertEquals(Integer.class, list.get(1).get().getClass());
		assertEquals(String.class, list.get(2).get().getClass());
		assertEquals(Date.class, list.get(3).get().getClass());
		assertTrue(list.get(4).get() instanceof Calendar);
	}
	
}
