/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

/**
 * @author h2b
 *
 */
public class ExcelUtilTest {

	/**
	 * Test method for {@link de.h2b.java.lib.office.ExcelUtil#dateOf(double)}.
	 * @throws ParseException 
	 */
	@Test
	public final void testDateOf() throws ParseException {
		double testDouble = 42046;
		Date expected = new SimpleDateFormat("dd.MM.yy").parse("11.02.2015");
		assertEquals(expected, ExcelUtil.dateOf(testDouble));
	}

}
