/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DateUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public class ExcelWriterTest {
	
	private static final String TEST_TABLE_NAME = "Test Table";
	
	private static final Date TEST_DATE = new Date();
	
	private static final Calendar TEST_CALENDAR = Calendar.getInstance();
	
	private List<List<TableElement<?>>> testContent;
	
	private  File testFile;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		testFile = File.createTempFile("ExcelWriterTest-", ".xslx");
		testContent = new ArrayList<List<TableElement<?>>>(6);
		List<TableElement<?>> header = new ArrayList<TableElement<?>>(6);
		header.add(TableElement.of("String Column"));
		header.add(TableElement.of("Boolean Column"));
		header.add(TableElement.of("Integer Column"));
		header.add(TableElement.of("Double Column"));
		header.add(TableElement.of("Date Column"));
		header.add(TableElement.of("Calendar Column"));
		testContent.add(header);
		for (int i = 1; i < 5; i++) {
			List<TableElement<?>> row = new ArrayList<TableElement<?>>(4);
			row.add(TableElement.of(i + ". row containing data with a first cell of considerable width to test auto-sizing of columns"));
			row.add(TableElement.of(i%2==0));
			row.add(TableElement.of(i));
			row.add(TableElement.of(i/2.0));
			row.add(TableElement.of(TEST_DATE));
			row.add(TableElement.of(TEST_CALENDAR));
			testContent.add(row);
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link de.h2b.java.lib.office.ExcelWriter#write()}
	 * in non-appending mode.
	 * 
	 * @throws IOException 
	 * @throws OfficeFormatException 
	 * @throws ParseException 
	 */
	@Test
	public void testWrite() throws IOException, OfficeFormatException {
		ExcelWriter writer = new ExcelWriter(testFile);
		writer.setMaxColumnWidth(40);
		writer.put(TEST_TABLE_NAME, testContent);
		writer.write();
		//check some spots
		ExcelReader reader = new ExcelReader(testFile);
		List<Map<String, String>> content = reader.getContent();
		String probe = content.get(1).get("Integer Column");
		assertEquals(2, Math.round(Double.valueOf(probe)));
		probe = content.get(2).get("Date Column");
		assertEquals(TEST_DATE, DateUtil.getJavaDate(Double.valueOf(probe)));
		//do further manual assertion for formatting
	}
	
	/**
	 * Test method for {@link de.h2b.java.lib.office.ExcelWriter#write()}
	 * in appending mode.
	 * 
	 * @throws OfficeFormatException 
	 * @throws IOException 
	 */
	@Test
	public void testAppend() throws IOException, OfficeFormatException {
		//create new file
		ExcelWriter writer = new ExcelWriter(testFile);
		writer.put(TEST_TABLE_NAME, testContent);
		writer.write();
		//append another table
		writer = new ExcelWriter(testFile, true);
		testContent = new ArrayList<List<TableElement<?>>>(2);
		List<TableElement<?>> header = new ArrayList<TableElement<?>>(2);
		header.add(TableElement.of("First String Column"));
		header.add(TableElement.of("Sercond String Column"));
		testContent.add(header);
		List<TableElement<?>> row = new ArrayList<TableElement<?>>(2);
		row.add(TableElement.of("this"));
		row.add(TableElement.of("that"));
		testContent.add(row);
		writer.put("Another Table", testContent);
		//append some data to existing table
		testContent = new ArrayList<List<TableElement<?>>>(2);
		for (int i = 5; i < 7; i++) {
			row = new ArrayList<TableElement<?>>(4);
			row.add(TableElement.of(i + ". row containing data with a first cell of considerable width to test auto-sizing of columns"));
			row.add(TableElement.of(i%2==0));
			row.add(TableElement.of(i));
			row.add(TableElement.of(i/2.0));
			row.add(TableElement.of(TEST_DATE));
			row.add(TableElement.of(TEST_CALENDAR));
			testContent.add(row);
		}
		writer.put(TEST_TABLE_NAME, testContent);
		//write it and check some spots
		writer.write();
		ExcelReader reader = new ExcelReader(testFile, 0);
		List<Map<String, String>> content = reader.getContent();
		String probe = content.get(5).get("Integer Column");
		assertEquals(6, Math.round(Double.valueOf(probe)));
		reader = new ExcelReader(testFile, 1);
		content = reader.getContent();
		probe = content.get(0).get("Sercond String Column");
		assertEquals("that", probe);
	}

}
