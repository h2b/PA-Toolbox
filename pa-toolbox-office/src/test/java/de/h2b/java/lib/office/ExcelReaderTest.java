/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author h2b
 *
 */
public class ExcelReaderTest {
	
	private static final String TEST_FILENAME = "ExcelReaderTest.xlsx";
	
	private static final Map<Integer, String> TEST_KEYS = new HashMap<Integer, String>();
	
	static {
		TEST_KEYS.put(0, "ColumnA");
		TEST_KEYS.put(1, "ColumnB");
		TEST_KEYS.put(2, "ColumnC");
		TEST_KEYS.put(4, "ColumnE");
	}
	
	private static final List<Map<String, String>> TEST_CONTENT = new ArrayList<Map<String, String>>();
	
	static {
		Map<String, String> row;
		row = new HashMap<String, String>();
		row.put("ColumnA", "cellA2");
		row.put("ColumnB", "cellB2");
		row.put("ColumnC", null);
		row.put("ColumnE", "cellE2");
		TEST_CONTENT.add(row);
		row = new HashMap<String, String>();
		row.put("ColumnA", "cellA3");
		row.put("ColumnB", "cellB3");
		row.put("ColumnC", "cellC3");
		row.put("ColumnE", null);
		TEST_CONTENT.add(row);
		row = new HashMap<String, String>();
		row.put("ColumnA", null);
		row.put("ColumnB", "cellB4");
		row.put("ColumnC", "cellC4");
		row.put("ColumnE", "cellE4");
		TEST_CONTENT.add(row);
	}
	
	private InputStream testStream;
	
	@Before
	public void setUp() {
		testStream = ExcelReaderTest.class.getResourceAsStream(TEST_FILENAME);
	}
	
	@After
	public void tearDown() throws IOException {
		testStream.close();
	}
	
	/**
	 * Test method for {@link de.h2b.java.lib.office.ExcelReader#getContent()}.
	 * @throws IOException 
	 * @throws OfficeFormatException 
	 */
	@Test
	public void testGetContent() throws OfficeFormatException, IOException {
		ExcelReader reader = new ExcelReader(testStream);
		List<Map<String, String>> actuals = reader.getContent();
		assertEquals(TEST_CONTENT.size(), actuals.size());
		for (Map<String, String> expected : TEST_CONTENT) {
			//search for expected in actuals
			boolean found = false;
			for (Map<String, String> actual : actuals) {
				//tests if expected equals actual
				if (expected.size()!=actual.size()) {
					continue; //no equality, consider next map
				}
				boolean isEqual = true;
				for (String key : expected.keySet()) {
					//check value for key
					String expectedVal = expected.get(key);
					String actualVal = actual.get(key);
					if (expectedVal==null && actualVal==null) {
						continue; //equal values, check next key
					}
					if (expectedVal==null && actualVal!=null || !expectedVal.equals(actualVal)) {
						//unequal values, so actual is no candidate any more
						isEqual = false;
						break;
					}
				}
				if (isEqual) {
					//nothing broke equality check, so we found expected
					found = true;
					break;
				}
			}
			assertTrue("Expected map not found in actuals.", found);
		}
	}

	/**
	 * Test method for {@link de.h2b.java.lib.office.ExcelReader#getKeys()}.
	 * @throws IOException 
	 * @throws OfficeFormatException 
	 */
	@Test
	public void testGetKeys() throws OfficeFormatException, IOException {
		ExcelReader reader = new ExcelReader(testStream);
		Map<Integer, String> actual = reader.getKeys();
		assertEquals(TEST_KEYS.size(), actual.size());
		for (int i : TEST_KEYS.keySet()) {
			assertTrue("Key " + i, TEST_KEYS.get(i).equals(actual.get(i)));
		}
	}

}
