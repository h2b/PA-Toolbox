/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import java.util.Date;

import org.apache.poi.ss.usermodel.DateUtil;

/**
 * Wrapper for utilities to eliminate some Excel dependencies of callers. 
 * 
 * @author h2b
 *
 */
public class ExcelUtil {
	
	/**
	 * Converts Excel's internal date format, which is the number of days since 
	 * 1900-01-01 formatted as double value.
	 * 
	 * @param value
	 * @return Java representation of the date, or 
	 *         {@code null} if value is not a valid Excel date
	 */
	public static Date dateOf(double value) {
		return DateUtil.getJavaDate(value);
	}

}
