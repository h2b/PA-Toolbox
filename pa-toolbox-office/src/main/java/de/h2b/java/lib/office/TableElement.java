/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.office;

import java.util.Calendar;
import java.util.Date;

/**
 * Container that holds a value of a certain type. Since only a restricted set
 * of types is allowed, the constructor is made private and several static
 * factory methods are provided instead to create new objects.
 * 
 * @author h2b
 *
 */
public final class TableElement<T> {
	
	private final T value;

	private TableElement(T value) {
		super();
		this.value = value;
	}
	
	public static TableElement<Boolean> of(boolean value) {
		return new TableElement<Boolean>(value);
	}
	
	public static <N extends Number> TableElement<N> of(N value) {
		return new TableElement<N>(value);
	}
	
	public static TableElement<String> of(String value) {
		return new TableElement<String>(value);
	}
	
	public static TableElement<Date> of(Date value) {
		return new TableElement<Date>(value);
	}
	
	public static TableElement<Calendar> of(Calendar value) {
		return new TableElement<Calendar>(value);
	}

	/**
	 * @return the value
	 */
	public T get() {
		return value;
	}
	
}
