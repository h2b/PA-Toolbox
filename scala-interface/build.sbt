name := "pa-toolbox-scala-interface"

scalaVersion in ThisBuild := "2.12.4"	

lazy val root = (project in file(".")).
    aggregate(pa_toolbox_scala_linalg, pa_toolbox_scala_plotting)

lazy val pa_toolbox_scala_linalg = (project in file("pa-toolbox-linalg")).
    settings(
      commonSettings
    )
    
lazy val pa_toolbox_scala_plotting = (project in file("pa-toolbox-plotting")).
    dependsOn(pa_toolbox_scala_linalg).
    settings(
      commonSettings
    )

lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % "3.0.4" % Test,
    "junit" % "junit" % "4.12" % Test
  )
)
