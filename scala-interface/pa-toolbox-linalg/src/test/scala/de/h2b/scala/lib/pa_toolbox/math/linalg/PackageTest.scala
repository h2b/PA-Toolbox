/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.math.linalg

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.java.lib.math.linalg.{ DoubleMatrix, DoubleVector,
  Function ⇒ paFunction, Matrices, Matrix ⇒ paMatrix, Vector => paVector,
  Vectors }
import de.h2b.scala.lib.math.{ DoubleApproxEqual, doubleTolerance }
import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }
import de.h2b.scala.lib.pa_toolbox.math.linalg.ImplicitConversions._

@RunWith(classOf[JUnitRunner])
class PackageTest extends FunSuite {

  private val u = Vector(1.0,2.0,3.0)@@0
  private val pau = new DoubleVector(Array(1.0,2.0,3.0))

  private val a = Matrix(Vector(11.0,12.0,13.0), Vector(21.0,22.0,23.0))@@(0,0)
  private val paa = new DoubleMatrix(Array(
      Array(11.0,12.0,13.0),
      Array(21.0,22.0,23.0)))

  private val f = (x: Double) ⇒ -x
  private val paf = new paFunction() {
    @Override
    def apply (value: Double): Double = -value
  }

  private def approx (v: Vector[Double], pav: paVector[java.lang.Double]): Boolean = {
    var result = (v.index.low==0) && (v.length==pav.length())
    for (i ← v.index) result = result && (v(i)~=pav.get(i))
    result
  }

  private def approx (a: Matrix[Double], paa: paMatrix[java.lang.Double]): Boolean = {
    var result = (a.index.dim1.low==0) && (a.index.dim2.low==0) &&
        (a.height==paa.height()) && (a.width==paa.width())
    for (i ← a.index.dim1) result = result && approx(a(i), paa.row(i))
    result
  }

  test("prerequisites are okay") {
    assert(approx(u, pau))
    assert(!approx(u@@1, pau))
    assert(!approx(Vector(1.0,2.0)@@0, pau))
    assert(!approx(Vector(1.0,2.1,3.0)@@0, pau))
    assert(approx(a, paa))
    assert(!approx(a atRow 1, paa))
    assert(!approx(Matrix(Vector(11.0,12.0,13.0))@@(0,0), paa))
    assert(!approx(Matrix(Vector(11.0,12.0,13.0), Vector(21.0,22.2,23.0))@@(0,0), paa))
  }

  test("Vector conversions explicit") {
    assertResult(u)(pau.asLa)
    assertResult(pau)(u.asPa)
  }

  test("Vector conversions implicit") {
    assert(u.norm ~= Vectors.euklidianNorm(u))
    assert(u.maxBy(_.abs) ~= Vectors.maximumNorm(u))
    assert((u*u) ~= Vectors.scalarProduct(pau, u))
    assert(approx(u+u, Vectors.sum(u, pau)))
    assert(approx(u*2, Vectors.product(2, u)))
  }

  test("Matrix conversions explicit") {
    assertResult(a)(paa.asLa)
    assertResult(paa)(a.asPa)
  }

  test("Matrix conversions implicit") {
    assert(approx(a.transposed(), Matrices.transpose(a)))
    assert(approx(a+a, Matrices.sum(a, paa)))
    assert(approx(a*a, Matrices.product(paa, a)))
    assert(approx(a*u, Matrices.product(a, pau)))
    assert(approx(a.rowSum(), Matrices.rowsum(a)))
    assert(approx(a.colSum(), Matrices.colsum(a)))
    assert(approx(a*2, Matrices.product(2, a)))
  }

  test("Function conversions explicit") {
    val testSeq = Seq(-3.5, -Math.PI, 0.0, 1.234, Math.PI)
    for (x ← testSeq) assert(-x ~= f.asPa(x))
    for (x ← testSeq) assert(-x ~= paf.asLa(x))
  }

  test("Function conversions implicit") {
    assert(approx(-u, Vectors.application(f, u)))
    assert(approx(-a, Matrices.application(f, a)))
  }

}
