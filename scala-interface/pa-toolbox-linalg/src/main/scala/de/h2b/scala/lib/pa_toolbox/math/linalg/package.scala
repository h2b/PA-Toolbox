/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.math

import de.h2b.java.lib.math.linalg.{ DoubleMatrix, DoubleVector, Function }
import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }

/**
 * Provides conversions for vectors, matrices and functions between this
 * PA-Toolbox linear-algebra Java project (`pa`) and the LinAlg Scala
 * library project (`la`).
 *
 * This package object adds converters `as..` to the vector, matrix and
 * function types of both projects. To use them, include an
 * `import de.h2b.scala.lib.pa_toolbox.math.linalg._` statement.
 *
 * If you prefer implicit conversions, use the `ImplicitConversions` object.
 *
 * @note PA-Toolbox currently only implements the element type `Double` for
 * vectors and matrices. Thus, on the LinAlg side only `Vector[Double]` and
 * `Matrix[Double]` are provided.
 *
 * @note PA-Toolbox requires that lower indices are 0. Thus, on the LinAlg
 * side only `Vector` and `Matrix` instances that fulfil this requirement are
 * provided.
 *
 * @author h2b
 */
package object linalg {

	/**
   * @throws IllegalArgumentException if the argument's lower index is not 0
	 */
	implicit class LaVectorOps (private val v: Vector[Double]) {
    def asPa: DoubleVector = {
  		require(v.index.low==0, "lower index must be 0")
  		new DoubleVector(v.toArray)
    }
  }

  implicit class PaVectorOps (private val pa: DoubleVector) {
    def asLa: Vector[Double] = {
      val a = pa.asArray()
      val b = a map { x: java.lang.Double => Double.unbox(x) }
      Vector(b: _*)@@0
    }
  }

	/**
   * @throws IllegalArgumentException if the argument's lower indices are not 0
	 */
  implicit class LaMatrixOps (private val a: Matrix[Double]) {
    def asPa: DoubleMatrix = {
      require(a.index.dim1.low==0 && a.index.dim2.low==0, "lower indices must be 0")
      val aa = a.toArray map { row: Vector[Double] ⇒ row.toArray }
      new DoubleMatrix(aa)
    }
  }

  implicit class PaMatrixOps (private val pa: DoubleMatrix) {
    def asLa: Matrix[Double] = {
      val aa = pa.asArray()
      val a = aa map { line: Array[java.lang.Double] => new DoubleVector(line) }
      val b = a map { row: DoubleVector ⇒ row.asLa }
      Matrix(b: _*) atRow 0
    }
  }

  implicit class LaFunctionOps (private val f: Function1[Double, Double]) {
    def asPa: Function = new Function() {
      @Override
      def apply (value: Double): Double = f(value)
    }
  }

  implicit class PaFunctionOps (private val pa: Function) {
    def asLa: Function1[Double, Double] = x ⇒ pa(x)
  }

}
