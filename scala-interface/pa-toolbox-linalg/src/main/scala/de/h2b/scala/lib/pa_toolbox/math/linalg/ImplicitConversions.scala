/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.math.linalg

import scala.language.implicitConversions

import de.h2b.java.lib.math.linalg.{ DoubleMatrix, DoubleVector, Function }
import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }

/**
 * Provides implicit conversions for vectors, matrices and functions between
 * this PA-Toolbox linear-algebra Java project (`pa`) and the LinAlg Scala
 * library project (`la`).
 *
 * To use these implicit conversions, include an
 * `import de.h2b.scala.lib.pa_toolbox.math.linalg.ImplicitConversions._`
 * statement.
 *
 * @author h2b
 */
object ImplicitConversions {

	/**
   * @throws IllegalArgumentException if the argument's lower index is not 0
	 */
  implicit def la2paVector (v: Vector[Double]): DoubleVector = v.asPa

  implicit def pa2laVector (pa: DoubleVector): Vector[Double] = pa.asLa

	/**
   * @throws IllegalArgumentException if the argument's lower indices are not 0
	 */
  implicit def la2paMatrix (a: Matrix[Double]): DoubleMatrix = a.asPa

  implicit def pa2laMatrix (pa: DoubleMatrix): Matrix[Double] = pa.asLa

  implicit def la2paFunction (f: Function1[Double, Double]): Function = f.asPa

  implicit def pa2laFunction (pa: Function): Function1[Double, Double] = pa.asLa

}
