name in ThisBuild := "pa-toolbox-linalg-scala"

libraryDependencies in ThisBuild ++= Seq(
  "de.h2b.java.lib.pa-toolbox" % "pa-toolbox-linalg" % "1.1.0",
  "de.h2b.scala.lib.math" %% "linalg" % "3.1.0",
  "de.h2b.scala.lib" %% "utilib" % "0.4.1"
)
