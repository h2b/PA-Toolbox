organization in ThisBuild := "de.h2b.scala.lib.pa-toolbox"

organizationName in ThisBuild := "private"

organizationHomepage in ThisBuild := Some(url("http://h2b.de"))

homepage in ThisBuild := Some(url("http://h2b.de"))

startYear in ThisBuild := Some(2017)

description in ThisBuild := """This is a Scala interface to
a Java library of some tools that happened to be useful in a 
predictive-analytics environment. Though, most are not specific to this area 
and might be of general interest.
"""

licenses in ThisBuild := 
	Seq("Apache License, Version 2.0" -> 
		url("https://www.apache.org/licenses/LICENSE-2.0"))

pomExtra in ThisBuild := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/PA-Toolbox.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
