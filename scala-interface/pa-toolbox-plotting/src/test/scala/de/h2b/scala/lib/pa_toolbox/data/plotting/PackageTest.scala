/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.data.plotting

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.java.lib.data.plotting.AxisFunction
import de.h2b.scala.lib.math.{ DoubleApproxEqual, doubleTolerance }

@RunWith(classOf[JUnitRunner])
class PackageTest extends FunSuite {

  private val f = (i: Int) ⇒ i/2.0
  private val paf = new AxisFunction() {
    @Override
    def apply (i: Int): Double = i/2.0
  }

  test("Function conversions explicit") {
    val testSeq = Seq(-3, -2, 0, 5, 10)
    for (i ← testSeq) assert(i/2.0 ~= f.asPa(i))
    for (i ← testSeq) assert(i/2.0 ~= paf.asScala(i))
  }

}
