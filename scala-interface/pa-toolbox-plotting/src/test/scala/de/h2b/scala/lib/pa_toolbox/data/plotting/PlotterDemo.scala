/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.data.plotting

import scala.collection.JavaConverters._

import de.h2b.java.lib.data.plotting.{ GnuPlotter, Increment, Plotter }
import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }
import de.h2b.scala.lib.pa_toolbox.data.plotting.ImplicitConversions._
import de.h2b.scala.lib.pa_toolbox.math.linalg.ImplicitConversions._
import de.h2b.scala.lib.pa_toolbox.math.linalg._
import de.h2b.scala.lib.pa_toolbox.data.plotting._

/**
 * @author h2b
 */
object PlotterDemoScala extends App {

  {
		val plotter: Plotter = new GnuPlotter();
		plotter.setOutput(GnuPlotter.Terminal.WXT.toString());
		plotter.setCurveStyle(Plotter.CurveStyle.LINES_POINTS);
		plotter.setSurfaceStyle(Plotter.SurfaceStyle.SOLID);
		plotter.setContourStyle(Plotter.ContourStyle.BASE);
		plotter.setHidden3D(true);
		plotter.setTitle("PlotterDemo");
    demo2D(plotter)
		demo3D(plotter)
	}

  def demo2D (plotter: Plotter) = {
    val n = 50
		plotter.setXTitle("x");
		plotter.setYTitle("sin(x)");
    plotter.plot((x: Double) ⇒ Math.sin(x),
        -2*Math.PI to 2*Math.PI by 4*Math.PI/n);
		plotter.setYTitle("f(x)");
    plotter.plot(Seq(
        (x: Double) ⇒ Math.sin(x),
        (x: Double) ⇒ 2*Math.cos(x),
        (x: Double) ⇒ 3*Math.sin(x)*Math.cos(x)),
        -2*Math.PI to 2*Math.PI by 4*Math.PI/n,
        Seq("f=sin", "f=2*cos", "f=3*sin*cos"))
  }

  def demo3D (plotter: Plotter) = {
    val m = 10
    val n = 20
    plotter.setYTitle("Δ")
    plotter.setZTitle("sin(4πx+Δ)")
    val xr = 0.0 to 1.0 by 1.0/n
    val yr = 0.0 to m-1 by Math.PI/4
    plotter.plot2D(f3d, xr, yr)
    plotter.plot3D(f3d, xr, yr)
  }

  private def f (delta: Double): Function1[Double, Double] =
    (x: Double) => Math.sin(4*Math.PI*x+delta)

  private def f3d (): Function2[Double, Double, Double] =
    (x: Double, y: Double) ⇒ f(y)(x)

}
