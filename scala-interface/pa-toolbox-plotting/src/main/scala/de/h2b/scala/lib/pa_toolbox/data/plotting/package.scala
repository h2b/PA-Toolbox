/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.data

import scala.collection.JavaConverters.seqAsJavaListConverter
import scala.collection.immutable.NumericRange

import de.h2b.java.lib.data.plotting.{ AxisFunction, Plotter ⇒ paPlotter }
import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }
import de.h2b.scala.lib.pa_toolbox.math.linalg.{ LaMatrixOps, LaVectorOps }

/**
 * Adds function-based plotting methods to the `Plotter` interface of the
 * PA-Toolbox plotting Java project.
 *
 * Note that you still can use the vector- and matrix-based versions of the Java
 * project by using the conversions of the pa-toolbox-linalg-scala project; to
 * do this include an `import de.h2b.scala.lib.pa_toolbox.math.linalg._` or
 * `import de.h2b.scala.lib.pa_toolbox.math.linalg.ImplicitConversions._`
 * statement.
 *
 * Also provides conversions for axis functions between this PA-Toolbox
 * data-plotting Java project (`pa`) and the Scala functionality (`scala`).
 * This package object adds converters `as..` to the function types of both
 * domains. To use them, include an
 * `import de.h2b.scala.lib.pa_toolbox.data.plotting._` statement.
 * If you prefer implicit conversions, use the `ImplicitConversions` object.
 *
 * @author h2b
 */
package object plotting {

  implicit class PlotterOps (private val plotter: paPlotter) {

    private def vec (f: Function1[Double, Double],
        range: NumericRange[Double]): Vector[Double] = Vector(f, range)@@0

    private def mat (fs: Seq[Function1[Double, Double]],
        range: NumericRange[Double]): Matrix[Double] = Matrix(fs, range)@@(0,0)

    /**
     * Plots a 2D curve using the given function values on the y axis.
     *
     * @note Will override the property to be set by `setXValues` of this
     * plotter object.
     *
     * @param f the function to be plot
     * @param range the range of x values
     *
     * @throws PlottingException - if this plotter cannot fulfil this task
     */
    def plot (f: Function1[Double, Double], range: NumericRange[Double]): Unit = {
      plotter.setXValues(range.asPa)
      plotter.plot(vec(f, range).asPa)
    }

    /**
     * Plots multiple curves in a 2D-coordinate system using the given
     * function values on the y axis.
     *
     * With respect to the legend sequence, each curve is annotated by the
     * corresponding string. If it is smaller than the length of the function
     * sequence, the missing annotations become empty; if it is larger,
     * the superfluous annotations are ignored.
     *
     * @note Will override the property to be set by `setXValues` of this
     * plotter object.
     *
     * @param fs the sequence of functions to be plot
     * @param range the range of x values
     * @param legend list containing curve titles (optional)
     *
     *
     * @throws PlottingException - if this plotter cannot fulfil this task
     */
    def plot (fs: Seq[Function1[Double, Double]], range: NumericRange[Double],
        legend: Seq[String] = Seq.empty): Unit = {
      plotter.setXValues(range.asPa)
      plotter.plot(mat(fs, range).asPa, legend.asJava)
    }

    /**
     * Plots multiple 2D curves in a 3D-coordinate system using the given
     * function values for x and y on the z axis. Strictly speaking, for each
     * y,,k,, in `yRange` a separate 2D curve z = f(x, y,,k,,) is drawn with x
     * in `xRange`.
     *
     * @note Will override the properties to be set by `setXValues` and
     * `setYValues` of this plotter object.
     *
     * @note Since this plot actually produces 2D curves, `setSurfaceStyle` and
     * `setContourStyle` are not applicable here and will be ignored.
     *
     * @param f the function (x, y) → z to be plot
     * @param xRange the range of x values
     * @param yRange the range of y values
     *
     *
     * @throws PlottingException - if this plotter cannot fulfil this task
     */
    def plot2D (f: Function2[Double, Double, Double],
        xRange: NumericRange[Double], yRange: NumericRange[Double]): Unit = {
      val fs = for (y ← yRange) yield (x: Double) ⇒ f(x,y)
      plotter.setXValues(xRange.asPa)
      plotter.setYValues(yRange.asPa)
      plotter.plot2D(mat(fs, xRange).asPa)
    }

    /**
     * Plots a 3D surface using the given function values for x and y on the z
     * axis.
     *
     * @note Will override the properties to be set by `setXValues` and
     * `setYValues` of this plotter object.
     *
     * @param f the function (x, y) → z to be plot
     * @param xRange the range of x values
     * @param yRange the range of y values
     *
     *
     * @throws PlottingException - if this plotter cannot fulfil this task
     */
    def plot3D (f: Function2[Double, Double, Double],
        xRange: NumericRange[Double], yRange: NumericRange[Double]): Unit = {
      val fs = for (y ← yRange) yield (x: Double) ⇒ f(x,y)
      plotter.setXValues(xRange.asPa)
      plotter.setYValues(yRange.asPa)
      plotter.plot3D(mat(fs, xRange).asPa)
    }

  }

  implicit class ScalaAxisFunctionOps (private val f: Function1[Int, Double]) {
    def asPa: AxisFunction = new AxisFunction() {
      @Override
      def apply (i: Int): Double = f(i)
    }
  }

  implicit class PaAxisFunctionOps (private val pa: AxisFunction) {
    def asScala: Function1[Int, Double] = i ⇒ pa(i)
  }

}
