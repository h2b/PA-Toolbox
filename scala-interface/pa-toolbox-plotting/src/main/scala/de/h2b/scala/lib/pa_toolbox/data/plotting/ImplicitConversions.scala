/*
  PA-Toolbox - Predictive Analytics Java Toolbox -- Scala Interface

  Copyright 2017-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.pa_toolbox.data.plotting

import scala.language.implicitConversions

import de.h2b.java.lib.data.plotting.AxisFunction

/**
 * Provides implicit conversions for axis functions between this PA-Toolbox
 * data-plotting Java project (`pa`) and the Scala functionality (`scala`).
 *
 * To use these implicit conversions, include an
 * `import de.h2b.scala.lib.pa_toolbox.data.plotting.ImplicitConversions._`
 * statement.
 *
 * @author h2b
 */
object ImplicitConversions {

  implicit def scala2paFunction (f: Function1[Int, Double]): AxisFunction =
    f.asPa

  implicit def pa2scalaFunction (pa: AxisFunction): Function1[Int, Double] =
    pa.asScala

}
