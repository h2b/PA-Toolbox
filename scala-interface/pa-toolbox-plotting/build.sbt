name in ThisBuild := "pa-toolbox-plotting-scala"

libraryDependencies in ThisBuild ++= Seq(
  "de.h2b.java.lib.pa-toolbox" % "pa-toolbox-plotting" % "1.1.0",
  "de.h2b.scala.lib.math" %% "linalg" % "3.1.0"
)
