/*
  PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.h2b.java.lib.data.plotting;

import java.util.Arrays;

import de.h2b.java.lib.math.linalg.DoubleMatrix;
import de.h2b.java.lib.math.linalg.DoubleVector;
import de.h2b.java.lib.math.linalg.Matrix;
import de.h2b.java.lib.math.linalg.Vector;

/**
 * @author h2b
 *
 */
public class PlotterDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vector<Double> v = genvec(20, 0.0);
		Matrix<Double> a = genmat(10, 20);
		Plotter plotter = new GnuPlotter();
		plotter.setOutput(GnuPlotter.Terminal.WXT.toString());
		plotter.setCurveStyle(Plotter.CurveStyle.LINES_POINTS);
		plotter.setSurfaceStyle(Plotter.SurfaceStyle.SOLID);
		plotter.setContourStyle(Plotter.ContourStyle.BASE);
		plotter.setHidden3D(true);
		plotter.setTitle("PlotterDemo");
		plotter.setXValues(new Increment(0, 4*Math.PI/20));
		plotter.setXTitle("x");
		plotter.setYTitle("sin(x)");
		plotter.plot(v);
		plotter.plot(a, Arrays.asList("A","B","C","etc"));
		plotter.setYTitle("curve index");
		plotter.setZTitle("sin(x+d)");
		plotter.plot2D(a);
		plotter.plot3D(a);
	}

	private static Vector<Double> genvec(int n, double delta) {
		Vector<Double> v = new DoubleVector(n);
		for (int i = 0; i < n; i++) {
			v.set(i, Math.sin(4*Math.PI*i/n+delta));
		}
		return v;
	}

	private static Matrix<Double> genmat(int m, int n) {
		Matrix<Double> a = new DoubleMatrix(m, n);
		for (int i = 0; i < m; i++) {
			a.set(i, genvec(n, i*Math.PI/4));
		}
		return a;
	}

}
