/*
  PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.h2b.java.lib.data.plotting;

import java.io.File;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import de.h2b.java.lib.math.linalg.Matrix;
import de.h2b.java.lib.math.linalg.Vector;

import com.panayotis.gnuplot.GNUPlotException;
import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.JavaPlot.Key;
import com.panayotis.gnuplot.dataset.PointDataSet;
import com.panayotis.gnuplot.plot.Axis;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.FileTerminal;
import com.panayotis.gnuplot.terminal.GNUPlotTerminal;
import com.panayotis.gnuplot.utils.Debug;

/**
 * Implements the {@link Plotter} interface based on {@link JavaPlot} which in
 * turn uses Gnuplot as backend. Thus, when using this implementation, Gnuplot
 * must be installed on the system where the application runs.
 * 
 * @author h2b
 *
 */
public class GnuPlotter extends AbstractPlotter {
	
	/**
	 * Some useful terminals. Availability is system dependent: {@code QT} for
	 * Linux, {@code WINDOWS} for Windows, {@code WXT} for all with different
	 * capabilities.
	 * <p/>
	 * Use with {@code toString} and {@link Plotter#setOutput(String)}, since this
	 * enumeration is by far not complete and other terminals can be entered
	 * as plain string.
	 *
	 */
	public enum Terminal {
		QT, WINDOWS, WXT;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}
	
	private static final PlotStyle SOLID_SURFACE_STYLE = new PlotStyle(Style.PM3D);
	
	private static final Map<CurveStyle, PlotStyle> CURVE_STYLE_MAP;
	
	static {
		CURVE_STYLE_MAP = new EnumMap<CurveStyle, PlotStyle>(CurveStyle.class);
		CURVE_STYLE_MAP.put(CurveStyle.BOXES, new PlotStyle(Style.BOXES));
		CURVE_STYLE_MAP.put(CurveStyle.FILLED_CURVES, new PlotStyle(Style.FILLEDCURVES));
		CURVE_STYLE_MAP.put(CurveStyle.LINES, new PlotStyle(Style.LINES));
		CURVE_STYLE_MAP.put(CurveStyle.LINES_POINTS, new PlotStyle(Style.LINESPOINTS));
		CURVE_STYLE_MAP.put(CurveStyle.POINTS, new PlotStyle(Style.POINTS));
	}
	
	private static final Map<ContourStyle, String> CONTOUR_STYLE_MAP;
	
	static {
		CONTOUR_STYLE_MAP = new EnumMap<ContourStyle, String>(ContourStyle.class);
		CONTOUR_STYLE_MAP.put(ContourStyle.BASE, "base");
		CONTOUR_STYLE_MAP.put(ContourStyle.SURFACE, "surface");
		CONTOUR_STYLE_MAP.put(ContourStyle.ALL, "both");
		//no value required for NONE
	}

	private GNUPlotTerminal terminal;

	@Override
	public void plot(Vector<Double> v) {
		try {
			DataSetPlot dataSetPlot = new DataSetPlot(points(v));
			dataSetPlot.setPlotStyle(translate(curveStyle, CURVE_STYLE_MAP));
			dataSetPlot.setTitle("");
			JavaPlot javaPlot = new JavaPlot();
			javaPlot.addPlot(dataSetPlot);
			doPlot(javaPlot, false);
		} catch (GNUPlotException e) {
			throw new PlottingException(e);
		}
	}

	private double[][] points(Vector<Double> v) {
		int length = v.length();
		double[][] result = new double[length][2];
		for (int i = 0; i < length; i++) {
			result[i][0] = xValues.apply(i);
			result[i][1] = v.get(i);
		}
		return result;
	}

	private static <K,V> V translate(K style, Map<K, V> map) {
		V gnuStyle = map.get(style);
		if (gnuStyle==null) {
			throw new AssertionError(style);
		}
		return gnuStyle;
	}

	private void doPlot(JavaPlot javaPlot, boolean hasLegend) {
		javaPlot.setTitle(title);
		setAxisLabel(javaPlot, "x", xTitle);
		setAxisLabel(javaPlot,"y", yTitle);
		setAxisLabel(javaPlot,"z", zTitle);
		javaPlot.setKey(hasLegend? Key.OUTSIDE: Key.OFF);
		if (terminal!=null) {
			javaPlot.setTerminal(terminal);
		}
		javaPlot.plot();
	}

	private static void setAxisLabel(JavaPlot plot, String name, String title) {
		Axis axis = plot.getAxis(name);
		if (axis!=null) {
			axis.setLabel(title);
		}
	}

	@Override
	public void plot(Matrix<Double> a, List<String> legend) {
		if (legend==null) {
			legend = Collections.emptyList();
		}
		int size = a.height();
		int titleCount = legend.size();
		try {
			JavaPlot javaPlot = new JavaPlot();
			for (int i = 0; i < size; i++) {
				DataSetPlot dataSetPlot = new DataSetPlot(points(a.row(i)));
				dataSetPlot.setPlotStyle(translate(curveStyle, CURVE_STYLE_MAP));
				dataSetPlot.setTitle(i<titleCount? legend.get(i): "");
				javaPlot.addPlot(dataSetPlot);
			}
			doPlot(javaPlot, true);
		} catch (GNUPlotException e) {
			throw new PlottingException(e);
		}
	}

	@Override
	public void plot2D(Matrix<Double> a) {
		try {
			JavaPlot javaPlot = new JavaPlot(true);
			int j = 0;
			for (Vector<Double>	row : a) {
				PointDataSet<Double> dataSet = new PointDataSet<Double>();
				int i = 0;
				for (double value : row) {
					dataSet.addPoint(xValues.apply(i), yValues.apply(j), value);
					i++;
				}
				DataSetPlot dataSetPlot = new DataSetPlot(dataSet);
				dataSetPlot.setPlotStyle(translate(curveStyle, CURVE_STYLE_MAP));
				javaPlot.addPlot(dataSetPlot);
				j++;
			}
			doPlot(javaPlot, false);
		} catch (GNUPlotException e) {
			throw new PlottingException(e);
		}
	}

	@Override
	public void plot3D(Matrix<Double> a) {
		try {
			PointDataSet<Double> dataSet = new PointDataSet<Double>();
			int j = 0;
			for (Vector<Double>	row : a) {
				int i = 0;
				for (double value : row) {
					dataSet.addPoint(xValues.apply(i), yValues.apply(j), value);
					i++;
				}
				j++;
			}
			DataSetPlot dataSetPlot = new DataSetPlot(dataSet);
			dataSetPlot.setPlotStyle(translate(curveStyle, CURVE_STYLE_MAP));
			JavaPlot javaPlot = new JavaPlot(true);
			javaPlot.addPlot(dataSetPlot);
			set3DParams(javaPlot, dataSetPlot, a);
			doPlot(javaPlot, false);
		} catch (GNUPlotException e) {
			throw new PlottingException(e);
		}
	}

	private void set3DParams(JavaPlot javaPlot, DataSetPlot dataSetPlot, Matrix<Double> a) {
		switch (surfaceStyle) {
		case NONE: 
			break;
		case SOLID: 
			dataSetPlot.setPlotStyle(SOLID_SURFACE_STYLE);
			//intentionally fall through
		case GRID:
			javaPlot.set("dgrid3d", gridParams(a));
			break;
		default:
			throw new AssertionError(surfaceStyle);
		}
		if (contourStyle!=ContourStyle.NONE) {
			javaPlot.set("contour", translate(contourStyle, CONTOUR_STYLE_MAP));
		}
		if (hidden3D) {
			javaPlot.set("hidden3d", "");
		}
	}

	private String gridParams(Matrix<Double> a) {
		int nx = gridNX>0? gridNX: a.width();
		int ny = gridNY>0? gridNY: a.height();
		return nx + "," + ny;
	}

	/**
	 * Sets the Gnuplot terminal. The effect of an invalid string is undefined.
	 * <p/>
	 * In contrast to other methods {@code null} is allowed as argument. It will
	 * (re-)set the terminal to a system-dependent default.
	 * 
	 * @param device a valid Gnuplot terminal string
	 */
	@Override
	public void setOutput(String device) {
		if (device==null) {
			terminal = null;
		} else {
			terminal = new FileTerminal(device);
		}
	}

	/**
	 * Sets the Gnuplot terminal to the file of given type. 
	 * The effect of an invalid type is undefined.
	 * <p/>
	 * In contrast to other methods {@code null} is allowed as argument. It will
	 * (re-)set the terminal to a system-dependent default.
	 * 
	 * @param file
	 * @param type a valid Gnuplot terminal string
	 */
	@Override
	public void setOutput(File file, String type) {
		if (file==null || type==null) {
			terminal = null;
		} else {
			terminal = new FileTerminal(type, file.getAbsolutePath());
		}
	}
	
	/**
	 * Switches debug mode on. Allows chaining like 
	 * {@code Plotter plotter = new GnuPlotter().debug()}.
	 * 
	 * @return this gnu plotter
	 */
	public GnuPlotter debug() {
		setDebugMode();
		return this;
	}
	
	/**
	 * Switches debug mode on.
	 */
	public static void setDebugMode() {
		JavaPlot.getDebugger().setLevel(Debug.VERBOSE);
	}
	
}
