/*
  PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.h2b.java.lib.data.plotting;

import java.io.File;
import java.util.List;

import de.h2b.java.lib.math.linalg.Matrix;
import de.h2b.java.lib.math.linalg.Vector;

/**
 * Provides plotting methods for evaluation purposes based on {@code Vector}
 * and {@code Matrix} data. If an implementation does not support all settings,
 * it will silently ignore them or use some meaningful default, except for
 * methods which are explicitly documented as "optional", in which case an
 * {@code UnsupportedOperationException} will be thrown. That is, all
 * implementations are required to yield some meaningful results for the
 * {@code plot}, {@code plot2D} and {@code plot3D} methods, although the results
 * may not be exactly as desired; only in case there is a runtime error that prevents
 * one of these methods from completing its task, a {@code PlottingException}
 * is thrown.
 * <p/>
 * If not otherwise stated, all methods throw a {@code NullPointerException} if 
 * a {@code null} argument is entered.
 * 
 * @author h2b
 *
 */
public interface Plotter {
	
	enum CurveStyle {BOXES, FILLED_CURVES, LINES, LINES_POINTS, POINTS}
	
	enum SurfaceStyle {GRID, SOLID, NONE}
	
	enum ContourStyle {BASE, SURFACE, ALL, NONE}
	
	/**
	 * Plots a 2D curve using the vector's values on the y axis.
	 * The corresponding x values are derived from {@link #setXValues(AxisFunction)}.
	 * 
	 * @param v the y values
	 * @throws PlottingException if this plotter cannot fulfill this task
	 */
	void plot(Vector<Double> v);
	
	/**
	 * Plots multiple curves in a 2D-coordinate system using the matrix' row vectors
	 * (each for one curve) on the y axis,
	 * each row vector for one curve.
	 * <p/>
	 * The corresponding x values are derived from {@link #setXValues(AxisFunction)}.
	 * <p/>
	 * The legend serves as title list: each curve is annotated by the corresponding
	 * title of the list. If the list is smaller than the matrix' height, the missing titles
	 * become empty; if it is larger, the superfluous titles are ignored.
	 * 
	 * @param a matrix holding the row vectors with each containing the y values for one curve
	 * @param legend list containing curve titles (maybe {@code null})
	 * @throws PlottingException if this plotter cannot fulfill this task
	 */
	void plot(Matrix<Double> a, List<String> legend);
	
	/**
	 * Plots 2D curves in a 3D-coordinate system using the matrix' row vectors
	 * (each for one curve) on the z axis. A curve with row index {@code i} will 
	 * appear on the y coordinate given by {@code fy.apply(i)}, where {@code fy}
	 * is defined by {@code setYValues}.
	 * <p/>
	 * The corresponding x and y values are derived from {@link #setXValues(AxisFunction)}
	 * and {@link #setYValues(AxisFunction)}, respectively.
	 * <p/>
	 * Since this plot actually produces 2D curves, {@code setSurfaceStyle} and
	 * {@code setContourStyle} are not applicable here and will be ignored.
	 * 
	 * @param a matrix holding the row vectors with each containing the z values for one curve
	 * @throws PlottingException if this plotter cannot fulfill this task
	 */
	void plot2D(Matrix<Double> a);
	
	/**
	 * Plots a 3D curve using the matrix' row vectors on the z axis. A vector 
	 * with row index {@code i} will be assigned a y coordinate given by {@code fy.apply(i)}, 
	 * where {@code fy} is defined by {@code setYValues}.
	 * <p/>
	 * The corresponding x and y values are derived from {@link #setXValues(AxisFunction)}
	 * and {@link #setYValues(AxisFunction)}, respectively.
	 * 
	 * @param a matrix holding the row vectors with each containig the z values for the curve
	 * @throws PlottingException if this plotter cannot fulfill this task
	 */
	void plot3D(Matrix<Double> a);
	
	/**
	 * Provides values for the x axis. Defaults to {@code Increment(0,1)}.
	 * 
	 * @param fx
	 */
	void setXValues(AxisFunction fx);

	/**
	 * Provides values for the y axis of 3D plots. Will be ignored for 2D plots.
	 * Defaults to {@code Increment(0,1)}.
	 * 
	 * @param fx
	 */
	void setYValues(AxisFunction fy);
	
	/**
	 * Sets the title of the x axis. Defaults to empty string.
	 * 
	 * @param title
	 */
	void setXTitle(String title);
	
	/**
	 * Sets the title of the y axis. Defaults to empty string.
	 * 
	 * @param title
	 */
	void setYTitle(String title);
	
	/**
	 * Sets the title of the z axis. Will be ignored for 2D plots.
	 * Defaults to empty string.
	 * 
	 * @param title
	 */
	void setZTitle(String title);
	
	/**
	 * Defines the plotting style for curves. Defaults to {@code CurveStyle.LINES}.
	 * 
	 * @param style
	 */
	void setCurveStyle(CurveStyle style);
	
	/**
	 * Defines the plotting style for 3D sufaces. Will be ignored for 2D plots.
	 * Defaults to {@code SurfaceStyle.NONE}.
	 * 
	 * @param style
	 */
	void setSurfaceStyle(SurfaceStyle style);
	
	/**
	 * Defines the plotting style for 3D contours. Will be ignored for 2D plots.
	 * Defaults to {@code ContourStyle.NONE}.
	 * 
	 * @param style
	 */
	void setContourStyle(ContourStyle style);
	
	/**
	 * Defines parameters for automatic grid computing for 3D surfaces.
	 * <p/>
	 * A grid with dimensions derived from a bounding box of the data is created 
	 * for plotting and contouring. The grid is equally spaced in x and y dimension
	 * with {@code nx} and {@code ny} lines, respectively. The z values are 
	 * computed as weighted averages or spline interpolations of the data points' 
	 * z values.
	 * <p/>
	 * In other words, a regularly spaced grid is created and the a smooth approximation 
	 * to the raw data is evaluated for all grid points. 
	 * This approximation is plotted in place of the raw data.
	 * <p/>
	 * Will be ignored for 2D plots.
	 * Defaults to (0,0), i.e., the grid parameters will be derived from the
	 * matrix entered.
	 * 
	 * @param nx
	 * @param ny
	 * @throws IllegalArgumentException if one of the arguments is negative
	 */
	void setGrid(int nx, int ny);
	
	/**
	 * Sets hidden-line removal for 3D surface plotting on and off.
	 * Will be ignored for 2D plots.
	 * Defaults to {@code false}.
	 * 
	 * @param hidden
	 */
	void setHidden3D(boolean hidden);
	
	/**
	 * Sets the overall title of the plot. Defaults to empty string.
	 * 
	 * @param title
	 */
	void setTitle(String title);
	
	/**
	 * Redirects the output to some implementation-dependent device.
	 * Defaults to a common standard device.
	 * Optional operation.
	 * 
	 * @param device
	 * @throws UnsupportedOperationException if this plotter cannot redirect output 
	 *         to this or any device
	 */
	void setOutput(String device);
	
	/**
	 * Redirects the output to the specified file of given type.
	 * No default.
	 * Optional operation.
	 * 
	 * @param device
	 * @param type
	 * @throws UnsupportedOperationException if this plotter cannot redirect output
	 *         to this or any device
	 */
	void setOutput(File file, String type);

}
