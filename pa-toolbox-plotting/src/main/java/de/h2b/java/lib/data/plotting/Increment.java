/*
  PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.h2b.java.lib.data.plotting;

/**
 * @author h2b
 *
 */
public class Increment implements AxisFunction {
	
	private final double start;
	private final double step;

	/**
	 * Yields values beginning from {@code start} increased by {@code step}.
	 * 
	 * @param start
	 * @param step
	 */
	public Increment(double start, double step) {
		this.start = start;
		this.step = step;
	}

	@Override
	public double apply(int i) {
		return start+i*step;
	}

}
