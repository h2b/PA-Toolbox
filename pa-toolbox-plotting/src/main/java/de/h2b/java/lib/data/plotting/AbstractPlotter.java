/*
  PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
  Copyright 2015-2016 Hans-Hermann Bode
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package de.h2b.java.lib.data.plotting;

import java.io.File;

/**
 * Provides functionality and data common to all {@code Plotter} implementations.
 * 
 * @author h2b
 *
 */
public abstract class AbstractPlotter implements Plotter {
	
	protected AxisFunction xValues = new Increment(0, 1);
	protected AxisFunction yValues = new Increment(0, 1);
	
	protected String xTitle = "";
	protected String yTitle = "";
	protected String zTitle = "";
	
	protected CurveStyle curveStyle = CurveStyle.LINES;
	protected SurfaceStyle surfaceStyle = SurfaceStyle.NONE;
	protected ContourStyle contourStyle = ContourStyle.NONE;
	
	protected int gridNX = 0;
	protected int gridNY = 0;
	
	protected boolean hidden3D = false;
	
	protected String title = "";
	
	@Override
	public void setXValues(AxisFunction fx) {
		checkNull(fx);
		xValues = fx;
	}

	protected static void checkNull(Object o) {
		if (o==null) {
			throw new NullPointerException();
		}
	}

	@Override
	public void setYValues(AxisFunction fy) {
		checkNull(fy);
		yValues = fy;
	}

	@Override
	public void setXTitle(String title) {
		checkNull(title);
		xTitle = title;
	}

	@Override
	public void setYTitle(String title) {
		checkNull(title);
		yTitle = title;
	}

	@Override
	public void setZTitle(String title) {
		checkNull(title);
		zTitle = title;
	}

	@Override
	public void setCurveStyle(CurveStyle style) {
		checkNull(style);
		curveStyle = style;
	}

	@Override
	public void setSurfaceStyle(SurfaceStyle style) {
		checkNull(style);
		surfaceStyle = style;
	}

	@Override
	public void setContourStyle(ContourStyle style) {
		checkNull(style);
		contourStyle = style;
	}

	@Override
	public void setGrid(int nx, int ny) {
		checkNonNegative(nx);
		checkNonNegative(ny);
		gridNX = nx;
		gridNY = ny;
	}

	protected static void checkNonNegative(int n) {
		if (n<0) {
			throw new IllegalArgumentException("negative value entered: " + n);
		}
	}

	@Override
	public void setHidden3D(boolean hidden) {
		hidden3D = hidden;
	}

	@Override
	public void setTitle(String title) {
		checkNull(title);
		this.title = title;
	}

	@Override
	public void setOutput(String device) {
		throw new UnsupportedOperationException("output devices not supported");
	}

	@Override
	public void setOutput(File file, String type) {
		throw new UnsupportedOperationException("output files not supported");
	}

}
