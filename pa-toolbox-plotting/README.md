# PA-Toolbox -- Predictive-Analytics Java Toolbox -- Plotting Framework

Due to external dependencies, this submodule has a licence different from the parent project.

## License

PA-Toolbox - Predictive Analytics Java Toolbox - Plotting Framework
  
Copyright 2015-2016 Hans-Hermann Bode
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/]([http://www.gnu.org/licenses/).
