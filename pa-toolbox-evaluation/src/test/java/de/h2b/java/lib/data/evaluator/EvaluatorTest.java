/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.data.evaluator;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import de.h2b.java.lib.math.linalg.DoubleVector;

public final class EvaluatorTest {
	
	private Evaluator evaluator;

	@Before
	public void setUp() throws Exception {
		double[][] a = new double[][]{{1,2},{3,4}};
		double[][] b = new double[][]{{2,4},{4,6}};
		evaluator = new Evaluator(b, a);
	}

	@Test
	public void testRootMeanSquareDeviation() {
		assertEquals(new DoubleVector(Arrays.asList(1.0,2.0)), evaluator.rootMeanSquareDeviation());
	}

	@Test
	public void testNormalizedRootMeanSquareDeviation() {
		assertEquals(new DoubleVector(Arrays.asList(1.0/2.0,2.0/2.0)), evaluator.normalizedRootMeanSquareDeviation());
	}

	@Test
	public void testCoefficientOfVariationOfRootMeanSquareDeviation() {
		assertEquals(new DoubleVector(Arrays.asList(1.0/2.0,2.0/3.0)), evaluator.coefficientOfVariationOfRootMeanSquareDeviation());
	}

}
