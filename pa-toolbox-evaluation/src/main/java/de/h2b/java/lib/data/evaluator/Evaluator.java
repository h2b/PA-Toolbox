/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.data.evaluator;

import java.util.List;

import de.h2b.java.lib.math.linalg.DoubleMatrix;
import de.h2b.java.lib.math.linalg.DoubleVector;
import de.h2b.java.lib.math.linalg.Function;
import de.h2b.java.lib.math.linalg.Matrices;
import de.h2b.java.lib.math.linalg.Matrix;
import de.h2b.java.lib.math.linalg.Vector;

/**
 * Provides evaluation methods based on two matrices. One of the matrices is
 * supposed to hold the values to be evaluated, the other one serves as
 * comparison. in other words, one matrix is the probe that is tested against
 * the other one.
 * 
 * @author h2b
 *
 */
public class Evaluator {
	
	private final Matrix<Double> a, b;
	
	public Evaluator(Matrix<Double> a, Matrix<Double> b) {
		this.a = new DoubleMatrix(a);
		this.b = new DoubleMatrix(b);
	}

	public Evaluator(List<List<Double>> a, List<List<Double>> b) {
		this.a = new DoubleMatrix(a);
		this.b = new DoubleMatrix(b);
	}
	
	public Evaluator(double[][] a, double[][] b) {
		this.a = new DoubleMatrix(a);
		this.b = new DoubleMatrix(b);
	}

	/**
	 * @return {@code c} where for all {@code i ci=ai-bi}
	 */
	public Matrix<Double> deviation() {
		return Matrices.sum(a, Matrices.product(-1, b));
	}

	/**
	 * @return {@code c} where for all {@code i ci=(ai-bi)^2}
	 */
	public Matrix<Double> squareDeviation() {
		Matrix<Double> dev = deviation();
		return Matrices.application(new Function() {
			@Override
			public double apply(double value) {
				return value*value;
			}
		}, dev);
	}
	
	/**
	 * @return {@code RMSD(a,b)} for each column
	 */
	public Vector<Double> rootMeanSquareDeviation() {
		Matrix<Double> sqdev = squareDeviation();
		int m = sqdev.width();
		Vector<Double> result = new DoubleVector(m);
		for (int j = 0; j < m; j++) {
			Vector<Double> colj = sqdev.col(j);
			double sum = 0;
			for (int k = 0; k < colj.length(); k++) {
				sum += colj.get(k);
			}
			result.set(j, Math.sqrt(sum/colj.length()));
		}
		return result;
	}
	
	/**
	 * Note that elements of the resulting vector become infinite if the 
	 * corresponding column of {@code b} has values all equal.
	 * 
	 * @return {@code NRMSD(a,b)} for each column normalized to the range of 
	 * 			that column in {@code b}
	 */
	public Vector<Double> normalizedRootMeanSquareDeviation() {
		Vector<Double> result = rootMeanSquareDeviation();
		int len = result.length();
		for (int j = 0; j < len; j++) {
			result.set(j, result.get(j)/range(j)); //might become infinite
		}
		return result;
	}

	private double range(int j) throws IndexOutOfBoundsException {
		Vector<Double> colj = b.col(j);
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		for (double x : colj) {
			if (x<min) {
				min = x;
			}
			if (x>max) {
				max = x;
			}
		}
		return max-min;
	}

	/**
	 * Note that elements of the resulting vector become infinite if the 
	 * corresponding column of {@code b} has a zero mean.
	 * 
	 * @return {@code CV(NRMSD(a,b))} for each column normalized to the mean
	 * 			value of that column in {@code b}
	 */
	public Vector<Double> coefficientOfVariationOfRootMeanSquareDeviation() {
		Vector<Double> result = rootMeanSquareDeviation();
		int len = result.length();
		for (int j = 0; j < len; j++) {
			result.set(j, result.get(j)/mean(j)); //might become infinite
		}
		return result;
	}

	private double mean(int j) throws IndexOutOfBoundsException {
		Vector<Double> colj = b.col(j);
		double sum = 0;
		for (double x : colj) {
			sum += x;
		}
		return sum/colj.length();
	}

}
