/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public final class VectorsTest {

	private static final double EPSILON = 1e-8;

	private Vector<Double> v0;
	private Vector<Double> v1;
	private Vector<Double> v2;
	private Vector<Double> v3;

	@Before
	public void setUp() throws Exception {
		v0 = new DoubleVector(10);
		v1 = new DoubleVector(new double[]{1,2,3});
		v2 = new DoubleVector(new double[]{4,5});
		v3 = new DoubleVector(new double[]{-1,0,0});
	}

	@Test
	public void testSum() {
		assertEquals(new DoubleVector(new double[]{5,7,3}), Vectors.sum(v1, v2));
	}

	@Test
	public void testProduct() {
		assertEquals(new DoubleVector(new double[]{-2,-4,-6}), Vectors.product(-2, v1));
	}
	
	@Test
	public void testEuklidianNorm() {
		assertEquals(0, Vectors.euklidianNorm(v0), EPSILON);
		assertEquals(Math.sqrt(14), Vectors.euklidianNorm(v1), EPSILON);
		assertEquals(Math.sqrt(41), Vectors.euklidianNorm(v2), EPSILON);
		assertEquals(1, Vectors.euklidianNorm(v3), EPSILON);
	}

	@Test
	public void testMaximumNorm() {
		assertEquals(0, Vectors.maximumNorm(v0), EPSILON);
		assertEquals(3, Vectors.maximumNorm(v1), EPSILON);
		assertEquals(5, Vectors.maximumNorm(v2), EPSILON);
		assertEquals(1, Vectors.maximumNorm(v3), EPSILON);
	}
	
	@Test
	public void testScalarProduct() {
		assertEquals(0, Vectors.scalarProduct(v0, v1), EPSILON);
		assertEquals(14, Vectors.scalarProduct(v1, v2), EPSILON);
		assertEquals(-4, Vectors.scalarProduct(v2, v3), EPSILON);
}

}
