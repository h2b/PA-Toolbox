/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public final class MatricesTest {

	private Matrix<Double> m1;
	
	private Matrix<Double> m2;
	
	@Before
	public void setUp() throws Exception {
		m1 = new DoubleMatrix(new double[][]{{1,2},{3,4},{5,6}});
		m2 = new DoubleMatrix(new double[][]{{7,8,9},{10,11,12}});
	}

	@Test
	public void testSum() {
		assertEquals(new DoubleMatrix(new double[][]{{8,10,9},{13,15,12},{5,6,0}}), Matrices.sum(m1, m2));
	}

	@Test
	public void testProductMatrixScalar() {
		assertEquals(new DoubleMatrix(new double[][]{{-2,-4},{-6,-8},{-10,-12}}), Matrices.product(-2, m1));
	}
	
	@Test
	public void testTranspose() {
		assertEquals(new DoubleMatrix(new double[][]{{1,3,5},{2,4,6}}), Matrices.transpose(m1));
	}
	
	@Test
	public void testProductMatrixVector() {
		Vector<Double> v = new DoubleVector(new double[]{1,0});
		assertEquals(new DoubleVector(new double[]{1,3,5}), Matrices.product(m1, v));
	}
	
	@Test
	public void testProductMatrixMatrix() {
		Matrix<Double> a = new DoubleMatrix(new double[][]{{1,2},{3,4}});
		Matrix<Double> b = new DoubleMatrix(new double[][]{{1,0},{0,1}});
		assertEquals(a, Matrices.product(a, b));
	}
	
	@Test
	public void testRowSum() {
		assertEquals(new DoubleVector(new double[]{3,7,11}), Matrices.rowsum(m1));
		assertEquals(new DoubleVector(new double[]{24,33}), Matrices.rowsum(m2));
	}

	@Test
	public void testColSum() {
		assertEquals(new DoubleVector(new double[]{9,12}), Matrices.colsum(m1));
		assertEquals(new DoubleVector(new double[]{17,19,21}), Matrices.colsum(m2));
	}

}
