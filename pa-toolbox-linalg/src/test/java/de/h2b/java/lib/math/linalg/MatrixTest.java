/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public final class MatrixTest {

	private static final double EPSILON = 1e-5;
	
	private Matrix<Double> m0;
	
	private Matrix<Double> m1;
	
	private Matrix<Double> m2;
	
	@Before
	public void setUp() throws Exception {
		m0 = new DoubleMatrix(10, 10);
		m1 = new DoubleMatrix(new double[][]{{1,2},{3,4},{5,6}});
		m2 = new DoubleMatrix(new double[][]{{1.1,1.2,1.3},{2.1}});
	}

	@Test
	public void testRow() {
		assertEquals(new DoubleVector(10), m0.row(9));
		assertEquals(new DoubleVector(new double[]{1,2}), m1.row(0));
		assertEquals(new DoubleVector(new double[]{2.1,0,0}), m2.row(1));
	}

	@Test
	public void testCol() {
		assertEquals(new DoubleVector(10), m0.col(9));
		assertEquals(new DoubleVector(new double[]{1,3,5}), m1.col(0));
		assertEquals(new DoubleVector(new double[]{1.2,0}), m2.col(1));
	}

	@Test
	public void testSet() {
		m1.set(1, new DoubleVector(new double[]{7}));
		assertEquals(new DoubleVector(new double[]{7,0}), m1.row(1));
	}

	@Test
	public void testHeight() {
		assertEquals(10, m0.height());
		assertEquals(3, m1.height());
		assertEquals(2, m2.height());
	}

	@Test
	public void testWidth() {
		assertEquals(10, m0.width());
		assertEquals(2, m1.width());
		assertEquals(3, m2.width());
	}

	@Test
	public void testIterator() {
		double sum = 0;
		for (Vector<Double> row : m1) {
			for (double el : row) {
				sum += el;
			}
		}
		assertEquals(21, sum, EPSILON);
	}

	@Test
	public void testEqualsObject() {
		Matrix<Double> m1a = new DoubleMatrix(m1);
		assertTrue(m1a.equals(m1));
		m1a.set(1, new DoubleVector(2));
		assertFalse(m1a.equals(m1));
		assertFalse(m1a.equals(null));
	}

	@Test
	public void testToString() {
		System.out.println("testToString: " + m1);
		assertEquals("[[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]]", m1.toString());
	}

}
