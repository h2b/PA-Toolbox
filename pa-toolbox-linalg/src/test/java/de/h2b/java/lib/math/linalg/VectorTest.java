/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public final class VectorTest {
	
	private static final double EPSILON = 1e-5;
	
	private Vector<Double> v0;
	
	private Vector<Double> v1;
	
	private Vector<Double> v2;

	@Before
	public void setUp() throws Exception {
		v0 = new DoubleVector(10);
		v1 = new DoubleVector(new double[]{1,2,3});
		v2 = new DoubleVector(Arrays.asList(4.0,5.0,6.0,7.0));
	}

	@Test
	public void testGet() {
		assertEquals(0, v0.get(9), EPSILON);
		assertEquals(1, v1.get(0), EPSILON);
		assertEquals(5, v2.get(1), EPSILON);
	}

	@Test
	public void testSet() {
		v0.set(5, 42.0);
		assertEquals(42, v0.get(5), EPSILON);
	}

	@Test
	public void testLength() {
		assertEquals(10, v0.length());
		assertEquals(3, v1.length());
		assertEquals(4, v2.length());
	}

	@Test
	public void testIterator() {
		double sum = 0;
		for (Double el : v1) {
			sum += el;
		}
		assertEquals(6, sum, EPSILON);
	}

	@Test
	public void testEqualsObject() {
		Vector<Double> v1a = new DoubleVector(v1);
		assertTrue(v1a.equals(v1));
		v1a.set(1, 0.0);
		assertFalse(v1a.equals(v1));
		assertFalse(v1a.equals(null));
	}

	@Test
	public void testToString() {
		System.out.println("testToString: " + v1);
		assertEquals("[1.0, 2.0, 3.0]", v1.toString());
	}

}
