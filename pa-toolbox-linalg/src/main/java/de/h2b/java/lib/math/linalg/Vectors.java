/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

/**
 * Provides static operations on {@link Vector} objects.
 * Currently, only {@code Vector<Double>} is supported.
 * 
 * @author h2b
 *
 */
public class Vectors {
	
	/**
	 * @param v
	 * @return {@code ||v||}
	 */
	public static double euklidianNorm(Vector<Double> v) {
		return Math.sqrt(scalarProduct(v, v));
	}
	
	/**
	 * @param v
	 * @return {@code ||x||∞}
	 */
	public static double maximumNorm(Vector<Double> v) {
		double max = 0;
		for (double x : v) {
			double abs = Math.abs(x);
			if (abs>max) {
				max = abs;
			}
		}
		return max;
	}
	
	/**
	 * The shorter vector -- if any -- will be padded with zeros.
	 * 
	 * @param u
	 * @param v
	 * @return {@code u * v}
	 */
	public static double scalarProduct(Vector<Double> u, Vector<Double> v) {
		double s = 0;
		int minlen = Math.min(u.length(),  v.length());
		for (int i = 0; i < minlen; i++) {
			s += u.get(i)*v.get(i);
		}
		return s;
	}
	
	/**
	 * The shorter vector -- if any -- will be padded with zeros.
	 * 
	 * @param u
	 * @param v
	 * @return {@code u + v}
	 */
	public static Vector<Double> sum(Vector<Double> u, Vector<Double> v) {
		int ul = u.length();
		int vl = v.length();
		int wl = Math.max(ul, vl);
		Vector<Double> w = new DoubleVector(wl);
		for (int i = 0; i < wl; i++) {
			double ui = i<ul? u.get(i): 0;
			double vi = i<vl? v.get(i): 0;
			w.set(i, ui+vi);
		}
		return w;
	}
	
	/**
	 * @param s
	 * @param v
	 * @return {@code s * v}
	 */
	public static Vector<Double> product(final double s, Vector<Double> v) {
		return application(new Function() {
			@Override
			public double apply(double value) {
				return s*value;
			}
		}, v);
	}
	
	/**
	 * Returns a copy of the vector with the given function applied to each 
	 * element.
	 * 
	 * @param f
	 * @param v
	 * @return {@code f(v)}
	 */
	public static Vector<Double> application(Function f, Vector<Double> v) {
		int len = v.length();
		Vector<Double> w = new DoubleVector(len);
		for (int i = 0; i < len; i++) {
			w.set(i, f.apply(v.get(i)));
		}
		return w;
	}

}
