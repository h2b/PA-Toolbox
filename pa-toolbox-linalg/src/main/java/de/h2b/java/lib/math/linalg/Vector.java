/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import java.util.List;

/**
 * An algebraic vector consisting of {@code E} elements.
 * 
 * @author h2b
 *
 * @param <E> the element type
 */
public interface Vector<E> extends Iterable<E> {

	/**
	 * The index range is {@code [0, length-1]}.
	 * 
	 * @param i
	 * @return the element with index {@code i}
	 * @throws IndexOutOfBoundsException if index not in range
	 */
	E get(int i) throws IndexOutOfBoundsException;

	/**
	 * Sets the element with the specified index to the given value.
	 * The index range is {@code [0, length-1]}.
	 * 
	 * @param i the index
	 * @param x the value
	 * @throws IndexOutOfBoundsException if index not in range
	 */
	void set(int i, E x) throws IndexOutOfBoundsException ;

	/**
	 * @return the length of this vector
	 */
	int length();

	/**
	 * @return an array view of this vector
	 */
	E[] asArray();

	/**
	 * @return a list view of this vector
	 */
	List<E> asList();
	
}
