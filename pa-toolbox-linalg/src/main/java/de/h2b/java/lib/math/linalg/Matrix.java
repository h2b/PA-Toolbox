/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import java.util.List;

/**
 * An algebraic matrix consisting of {@code E} elements.
 * 
 * @author h2b
 *
 * @param <E> the element type
 */
public interface Matrix<E> extends Iterable<Vector<E>> {

	/**
	 * The index range is {@code [0, height-1]}.
	 * 
	 * @param i
	 * @return the row vector with index {@code i}
	 * @throws IndexOutOfBoundsException if index not in range
	 */
	Vector<E> row(int i) throws IndexOutOfBoundsException;

	/**
	 * The index range is {@code [0, width-1]}.
	 * 
	 * @param j
	 * @return the column vector with index {@code j}
	 * @throws IndexOutOfBoundsException if index not Tin range
	 */
	Vector<E> col(int j) throws IndexOutOfBoundsException;

	/**
	 * Sets the row with the specified index to the given vector.
	 * The index range is {@code [0, height-1]}.
	 * If the vector does not fit in the width of this matrix, it will be 
	 * truncated or padded with zeros, as appropriate.
	 * 
	 * @param i the index
	 * @param row the vector
	 * @throws IndexOutOfBoundsException if index not in range
	 */
	void set(int i, Vector<E> row) throws IndexOutOfBoundsException;

	/**
	 * @return the number of rows of this matrix
	 */
	int height();
	
	/**
	 * @return the number of columns of this matrix
	 */
	int width();

	/**
	 * @return an array view of this matrix
	 */
	E[][] asArray();

	/**
	 * @return a list view of this vector
	 */
	List<List<E>> asList();

}
