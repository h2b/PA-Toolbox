/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

/**
 * Provides static operations on {@link Matrix} objects.
 * Currently, only {@code Matrix<Double>} is supported.
 * 
 * @author h2b
 *
 */
public class Matrices {
	
	/**
	 * @param a
	 * @return {@code aT}
	 */
	public static Matrix<Double> transpose(Matrix<Double> a) {
		int height = a.width();
		int width = a.height();
		Matrix<Double> b = new DoubleMatrix(height, width);
		for (int i = 0; i < height; i++) {
			b.set(i, a.col(i));
		}
		return b;
	}
	
	/**
	 * In each dimension, the smaller matrix -- if any -- will be padded with 
	 * zeros.
	 * 
	 * @param a
	 * @param b
	 * @return {@code a+b}
	 */
	public static Matrix<Double> sum(Matrix<Double> a, Matrix<Double> b) {
		int ah = a.height();
		int bh = b.height();
		int ch = Math.max(ah, bh);
		Matrix<Double> c = new DoubleMatrix(ch, Math.max(a.width(), b.width()));
		for (int i = 0; i < ch; i++) {
			Vector<Double> ai = i<ah? a.row(i): new DoubleVector(0);
			Vector<Double> bi = i<bh? b.row(i): new DoubleVector(0);
			c.set(i, Vectors.sum(ai, bi));
		}
		return c;
	}
	
	/**
	 * The width of the first and the height of the second matrix will be
	 * adapted by padding with zeros as needed.
	 * 
	 * @param a
	 * @param b
	 * @return {@code a * b}
	 */
	public static Matrix<Double> product(Matrix<Double> a, Matrix<Double> b) {
		int height = a.height();
		int width = b.width();
		Matrix<Double> c = new DoubleMatrix(height, width);
		for (int i = 0; i < height; i++) {
			Vector<Double> row = new DoubleVector(width);
			Vector<Double> ai = a.row(i);
			for (int j = 0; j < width; j++) {
				row.set(j, Vectors.scalarProduct(ai, b.col(j)));
			}
			c.set(i, row);
		}
		return c;
	}
	
	/**
	 * The width of the matrix and the length of the vector will be
	 * adapted by padding with zeros as needed.
	 * 
	 * @param a
	 * @param v
	 * @return {@code a * v}
	 */
	public static Vector<Double> product(Matrix<Double> a, Vector<Double> v) {
		int length = a.height();
		Vector<Double> c = new DoubleVector(length);
		for (int i = 0; i < length; i++) {
			c.set(i, Vectors.scalarProduct(a.row(i), v));
		}
		return c;
	}
	
	/**
	 * @param a
	 * @return the vector of row sums of {@code a}
	 */
	public static Vector<Double> rowsum(Matrix<Double> a) {
		Vector<Double> v = Vectors.application(new Function() {
			@Override public double apply(double value) {
				return 1;
			}
		}, new DoubleVector(a.width()));
		return product(a, v);
	}
	
	/**
	 * @param a
	 * @return the vector of column sums of {@code a}
	 */
	public static Vector<Double> colsum(Matrix<Double> a) {
		return rowsum(transpose(a));
	}
	
	/**
	 * @param s
	 * @param a
	 * @return {@code s*a}
	 */
	public static Matrix<Double> product(final double s, Matrix<Double> a) {
		return application(new Function() {
			@Override public double apply(double value) {
				return s*value;
			}
		}, a);
	}
	
	/**
	 * Returns a copy of the matrix with the given function applied to each 
	 * element.
	 * 
	 * @param f
	 * @param a
	 * @return {@code f(a)}
	 */
	public static Matrix<Double> application(Function f, Matrix<Double> a) {
		int h = a.height();
		Matrix<Double> b = new DoubleMatrix(h, a.width());
		for (int i = 0; i < h; i++) {
			b.set(i, Vectors.application(f, a.row(i)));
		}
		return b;
	}

}
