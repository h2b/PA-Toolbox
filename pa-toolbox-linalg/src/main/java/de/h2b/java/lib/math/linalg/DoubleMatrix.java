/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * An algebraic matrix of fixed height consisting of {@code double} elements
 * organized by row vectors using defensive copies for all objects.
 * Obtaining column vectors is expensive in this implementation.
 * <p/>
 * {@code null} elements are not allowed. The iterator returned does not
 * support the remove operation.
 * 
 * @author h2b
 *
 */
public class DoubleMatrix implements Matrix<Double> {
	
	private final DoubleVector[] data;
	private final int height, width;

	/**
	 * Creates an empty matrix (filled with zeros) with the specified number of
	 * rows and columns.
	 * 
	 * @param numberOfRows
	 * @param numberOfColumns
	 */
	public DoubleMatrix(int numberOfRows, int numberOfColumns) {
		this.height = numberOfRows;
		this.width = numberOfColumns;
		this.data = new DoubleVector[numberOfRows];
		for (int i = 0; i < height; i++) {
			this.data[i] = new DoubleVector(width);
		}
	}

	/**
	 * Creates a new matrix from a copy of the given array. The resulting row
	 * vectors are padded to the maximum length of all elements of the first 
	 * array dimension.
	 * 
	 * @param array
	 */
	public DoubleMatrix(double[][] array) {
		this.height = array.length;
		this.width = maxWidth(array);
		this.data = new DoubleVector[height];
		for (int i = 0; i < height; i++) {
			this.data[i] = new DoubleVector(Arrays.copyOf(array[i], width));
		}
	}

	private static int maxWidth(double[][] array) {
		int max = Integer.MIN_VALUE;
		for (double[] ds : array) {
			int len = ds.length;
			if (len>max) {
				max = len;
			}
		}
		return max;
	}

	/**
	 * Creates a new matrix from a copy of the given array. The resulting row
	 * vectors are padded to the maximum length of all elements of the first 
	 * array dimension.
	 * 
	 * @param array
	 */
	public DoubleMatrix(Double[][] array) {
		this.height = array.length;
		this.width = maxWidth(array);
		this.data = new DoubleVector[height];
		for (int i = 0; i < height; i++) {
			DoubleVector row = new DoubleVector(width);
			Double[] ai = array[i];
			for (int j = 0; j < ai.length; j++) {
				row.set(j, ai[j]);;
			}
			this.data[i] = row;
		}
	}

	private static int maxWidth(Double[][] array) {
		int max = Integer.MIN_VALUE;
		for (Double[] ds : array) {
			int len = ds.length;
			if (len>max) {
				max = len;
			}
		}
		return max;
	}

	/**
	 * Creates a new matrix from a copy of the given list. The resulting row
	 * vectors are padded to the maximum size of all elements of the outer list.
	 * 
	 * @param list
	 */
	public DoubleMatrix(List<List<Double>> list) {
		this.height = list.size();
		this.width = maxWidth(list);
		this.data = new DoubleVector[height];
		for (int i = 0; i < height; i++) {
			DoubleVector vec = new DoubleVector(width);
			List<Double> sublist = list.get(i);
			for (int j = 0; j < sublist.size(); j++) {
				vec.set(j, sublist.get(j));
			}
			this.data[i] = vec;
		}
	}

	private static int maxWidth(List<List<Double>> list) {
		int max = Integer.MIN_VALUE;
		for (List<Double> ds : list) {
			int len = ds.size();
			if (len>max) {
				max = len;
			}
		}
		return max;
	}

	/**
	 * Creates a new matrix as a copy of another matrix.
	 * 
	 * @param matrix
	 */
	public DoubleMatrix(Matrix<Double> matrix) {
		this(matrix.asArray());
	}
	
	@Override
	public Double[][] asArray() {
		Double[][] array = new Double[height][];
		for (int i = 0; i < height; i++) {
			array[i] = data[i].asArray();
		}
		return array;
	}
	
	@Override
	public List<List<Double>> asList() {
		List<List<Double>> list = new ArrayList<List<Double>>(height);
		for (int i = 0; i < height; i++) {
			list.add(data[i].asList());
		}
		return list ;
	}

	@Override
	public Vector<Double> row(int i) throws IndexOutOfBoundsException {
		return new DoubleVector(data[i]);
	}

	@Override
	public Vector<Double> col(int j) throws IndexOutOfBoundsException {
		Vector<Double> col = new DoubleVector(height);
		for (int i = 0; i < height; i++) {
			col.set(i, data[i].get(j));
		}
		return col;
	}

	@Override
	public void set(int i, Vector<Double> row) throws IndexOutOfBoundsException {
		DoubleVector newrow = new DoubleVector(width);
		int copylen = Math.min(width, row.length());
		for (int j = 0; j < copylen; j++) {
			newrow.set(j, row.get(j));;
		}
		data[i] = newrow;
	}

	@Override
	public int height() {
		return height;
	}
	
	@Override
	public int width() {
		return width;
	}

	/**
	 * Iterates over row vectors. Does not support remove operation.
	 */
	@Override
	public Iterator<Vector<Double>> iterator() {
		return new Iterator<Vector<Double>>() {
			
			private int currentPosition = 0;
			
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Vector<Double> next() {
				return data[currentPosition++];
			}
			
			@Override
			public boolean hasNext() {
				return currentPosition<height;
			}
		};
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(data);
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof DoubleMatrix && Arrays.equals(this.data, ((DoubleMatrix)obj).data);
	}

	@Override
	public String toString() {
		return Arrays.toString(data);
	}
	
}