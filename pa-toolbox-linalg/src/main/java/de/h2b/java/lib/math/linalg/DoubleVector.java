/*
  PA-Toolbox -- Predictive Analytics Java Toolbox

  Copyright 2014-2016 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.java.lib.math.linalg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * An algebraic vector of fixed length consisting of {@code double} elements
 * using defensive copies for all objects.
 * <p/>
 * {@code null} elements are not allowed. The iterator returned does not
 * support the remove operation.
 * 
 * @author h2b
 *
 */
public class DoubleVector implements Vector<Double> {
	
	private final double[] data;
	private final int length;

	/**
	 * Creates an empty vector (filled with zeros) of the specified length.
	 * 
	 * @param length
	 */
	public DoubleVector(int length) {
		this.length = length;
		this.data = new double[this.length];
	}

	/**
	 * Creates a new vector from a copy of the given array.
	 * 
	 * @param array
	 */
	public DoubleVector(double[] array) {
		this.length = array.length;
		this.data = Arrays.copyOf(array, length);
	}

	/**
	 * Creates a new vector from a copy of the given array.
	 * 
	 * @param array
	 */
	public DoubleVector(Double[] array) {
		this(Arrays.asList(array));
	}

	/**
	 * Creates a new vector from a copy of the given list.
	 * 
	 * @param list
	 */
	public DoubleVector(List<Double> list) {
		this.length = list.size();
		this.data = new double[length];
		for (int i = 0; i < length; i++) {
			data[i] = list.get(i);
		}
	}
	
	/**
	 * Creates a new vector as a copy of another vector.
	 * 
	 * @param vector
	 */
	public DoubleVector(Vector<Double> vector) {
		this(vector.asArray());
	}
	
	@Override
	public Double[] asArray() {
		Double[] result = new Double[length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i];
		}
		return result;
	}
	
	@Override
	public List<Double> asList() {
		List<Double> list = new ArrayList<Double>(length);
		for (double x : data) {
			list.add(x);
		}
		return list ;
	}

	@Override
	public Double get(int i) throws IndexOutOfBoundsException {
		return data[i];
	}

	@Override
	public void set(int i, Double x) throws IndexOutOfBoundsException {
		data[i] = x;
	}

	@Override
	public int length() {
		return length;
	}

	/**
	 * Does not support remove operation.
	 */
	@Override
	public Iterator<Double> iterator() {
		return new Iterator<Double>() {
			
			private int currentPosition = 0;
			
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Double next() {
				return data[currentPosition++];
			}
			
			@Override
			public boolean hasNext() {
				return currentPosition<length;
			}
		};
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(data);
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof DoubleVector && Arrays.equals(this.data, ((DoubleVector)obj).data);
	}

	@Override
	public String toString() {
		return Arrays.toString(data);
	}
	
}